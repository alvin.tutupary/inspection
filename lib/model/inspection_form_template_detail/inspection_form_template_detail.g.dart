// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'inspection_form_template_detail.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

InspectionFormTemplateDetail _$InspectionFormTemplateDetailFromJson(
    Map<String, dynamic> json) {
  return InspectionFormTemplateDetail(
    pk: json['pk'] as String,
    pkInspectionFormTemplateHeader:
        json['pkInspectionFormTemplateHeader'] as String,
    pkInspectionResponse: json['pkInspectionResponse'] as String,
    seq: json['seq'] as int,
    question: json['question'] as String,
    model: json['model'] as String,
    isHasImage: json['isHasImage'] as bool,
    isDeleted: json['isDeleted'] as bool,
    createdAt: json['createdAt'] == null
        ? null
        : DateTime.parse(json['createdAt'] as String),
    createdBy: json['createdBy'] as String,
    updatedAt: json['updatedAt'] == null
        ? null
        : DateTime.parse(json['updatedAt'] as String),
    updatedBy: json['updatedBy'] as String,
    pkInspectionFormTemplateHeaderNavigation:
        json['pkInspectionFormTemplateHeaderNavigation'] == null
            ? null
            : InspectionFormTemplateHeader.fromJson(
                json['pkInspectionFormTemplateHeaderNavigation']
                    as Map<String, dynamic>),
    pkInspectionResponseNavigation:
        json['pkInspectionResponseNavigation'] == null
            ? null
            : InspectionResponse.fromJson(
                json['pkInspectionResponseNavigation'] as Map<String, dynamic>),
    inspectionTransactionDetail: (json['inspectionTransactionDetail'] as List)
        ?.map((e) => e == null
            ? null
            : InspectionTransactionDetail.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$InspectionFormTemplateDetailToJson(
        InspectionFormTemplateDetail instance) =>
    <String, dynamic>{
      'pk': instance.pk,
      'pkInspectionFormTemplateHeader': instance.pkInspectionFormTemplateHeader,
      'pkInspectionResponse': instance.pkInspectionResponse,
      'seq': instance.seq,
      'question': instance.question,
      'model': instance.model,
      'isHasImage': instance.isHasImage,
      'isDeleted': instance.isDeleted,
      'createdAt': instance.createdAt?.toIso8601String(),
      'createdBy': instance.createdBy,
      'updatedAt': instance.updatedAt?.toIso8601String(),
      'updatedBy': instance.updatedBy,
      'pkInspectionFormTemplateHeaderNavigation':
          instance.pkInspectionFormTemplateHeaderNavigation,
      'pkInspectionResponseNavigation': instance.pkInspectionResponseNavigation,
      'inspectionTransactionDetail': instance.inspectionTransactionDetail,
    };
