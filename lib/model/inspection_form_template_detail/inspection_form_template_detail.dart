import 'package:inspection/model/inspection/inspection.dart';
import 'package:inspection/model/inspection_form_template_header/inspection_form_template_header.dart';
import 'package:inspection/model/inspection_response/inspection_response.dart';
import 'package:inspection/model/inspection_response_detail/inspection_response_detail.dart';
import 'package:inspection/model/inspection_transaction_detail/inspection_transaction_detail.dart';

import 'package:json_annotation/json_annotation.dart';
part 'inspection_form_template_detail.g.dart';

@JsonSerializable()
class InspectionFormTemplateDetail {
  String pk;
  String pkInspectionFormTemplateHeader;
  String pkInspectionResponse;
  int seq;
  String question;
  String model;
  bool isHasImage;
  bool isDeleted;
  DateTime createdAt;
  String createdBy;
  DateTime updatedAt;
  String updatedBy;

  InspectionFormTemplateHeader pkInspectionFormTemplateHeaderNavigation;
  InspectionResponse pkInspectionResponseNavigation;
  List<InspectionTransactionDetail> inspectionTransactionDetail;

  String description;
  String responseText;
  String solution;
  InspectionResponseDetail response;
  List<InspectionTransactionDetailImage> images;

  InspectionFormTemplateDetail(
      {this.pk,
      this.pkInspectionFormTemplateHeader,
      this.pkInspectionResponse,
      this.seq,
      this.question,
      this.model,
      this.isHasImage,
      this.isDeleted,
      this.createdAt,
      this.createdBy,
      this.updatedAt,
      this.updatedBy,
      this.pkInspectionFormTemplateHeaderNavigation,
      this.pkInspectionResponseNavigation,
      this.inspectionTransactionDetail,
      this.description,
      this.solution,
      this.response,
      this.images});

  @override
  String toString() {
    return """
    InspectionFormTemplateDetail{
      pk: $pk,
      pkInspectionFormTemplateHeader: $pkInspectionFormTemplateHeader,
      pkInspectionResponse: $pkInspectionResponse,
      seq: $seq,
      question: $question,
      model: $model,
      isHasImage: $isHasImage,
      isDeleted: $isDeleted,
      createdAt: $createdAt,
      createdBy: $createdBy,
      updatedAt: $updatedAt,
      updatedBy: $updatedBy,
      pkInspectionFormTemplateHeaderNavigation: $pkInspectionFormTemplateHeaderNavigation,
      pkInspectionResponseNavigation: $pkInspectionResponseNavigation,
      inspectionTransactionDetail: $inspectionTransactionDetail
    }
    """;
  }

  factory InspectionFormTemplateDetail.fromJson(Map<String, dynamic> json) =>
      _$InspectionFormTemplateDetailFromJson(json);

  Map<String, dynamic> toJson() => _$InspectionFormTemplateDetailToJson(this);
}
