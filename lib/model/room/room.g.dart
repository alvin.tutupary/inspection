// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'room.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Room _$RoomFromJson(Map<String, dynamic> json) {
  return Room(
    pk: json['pk'] as String,
    pkFloor: json['pkFloor'] as String,
    name: json['name'] as String,
    isDeleted: json['isDeleted'] as bool,
    createdAt: json['createdAt'] as String,
    createdBy: json['createdBy'] as String,
    updatedAt: json['updatedAt'] as String,
    updatedBy: json['updatedBy'] as String,
    pkFloorNavigation: json['pkFloorNavigation'] == null
        ? null
        : Floor.fromJson(json['pkFloorNavigation'] as Map<String, dynamic>),
    ppmActivity: (json['ppmActivity'] as List)
        ?.map((e) =>
            e == null ? null : PpmActivity.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$RoomToJson(Room instance) => <String, dynamic>{
      'pk': instance.pk,
      'pkFloor': instance.pkFloor,
      'name': instance.name,
      'isDeleted': instance.isDeleted,
      'createdAt': instance.createdAt,
      'createdBy': instance.createdBy,
      'updatedAt': instance.updatedAt,
      'updatedBy': instance.updatedBy,
      'pkFloorNavigation': instance.pkFloorNavigation,
      'ppmActivity': instance.ppmActivity,
    };
