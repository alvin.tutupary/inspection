import 'package:inspection/model/floor/floor.dart';
import 'package:inspection/model/ppm/ppm.dart';
import 'package:json_annotation/json_annotation.dart';

part 'room.g.dart';

@JsonSerializable()
class Room {
  String pk;
  String pkFloor;
  String name;
  bool isDeleted;
  String createdAt;
  String createdBy;
  String updatedAt;
  String updatedBy;

  Floor pkFloorNavigation;
  List<PpmActivity> ppmActivity;

  Room({
    this.pk,
    this.pkFloor,
    this.name,
    this.isDeleted,
    this.createdAt,
    this.createdBy,
    this.updatedAt,
    this.updatedBy,
    this.pkFloorNavigation,
    this.ppmActivity,
  });

  @override
  String toString() {
    return """
    Room{
      pk: $pk,
      pkFloor: $pkFloor,
      name: $name,
      isDeleted: $isDeleted,
      createdAt: $createdAt,
      createdBy: $createdBy,
      updatedAt: $updatedAt,
      updatedBy: $updatedBy,
      pkFloorNavigation: $pkFloorNavigation,
      ppmActivity: $ppmActivity,
    }
    """;
  }

  factory Room.fromJson(Map<String, dynamic> json) => _$RoomFromJson(json);

  Map<String, dynamic> toJson() => _$RoomToJson(this);
}
