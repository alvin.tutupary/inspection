import 'package:flutter/material.dart';

class LoginRequest extends ChangeNotifier{
  String username;
  String password;
  LoginRequest({this.username, this.password});
  
  Map<String, dynamic> toJson() => {
    'username': this.username,
    'password': this.password
  };
}

class RefreshRequest extends ChangeNotifier{
  String token;
  String refreshToken;
  
  RefreshRequest({this.token, this.refreshToken});
  
  Map<String, dynamic> toJson() => {
    'token': this.token,
    'refreshToken': this.refreshToken
  };
}

class AuthResponse extends ChangeNotifier{
  String token;
  String refreshToken;

  AuthResponse({this.token, this.refreshToken });

  factory AuthResponse.fromJson(Map<String, dynamic> json){
    return AuthResponse(
      token: json['token'],
      refreshToken: json['refreshToken']
    );
  }
}
