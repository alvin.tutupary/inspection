// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'inspection_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

InspectionResponse _$InspectionResponseFromJson(Map<String, dynamic> json) {
  return InspectionResponse(
    pk: json['pk'] as String,
    name: json['name'] as String,
    isDeleted: json['isDeleted'] as bool,
    createdAt: json['createdAt'] as String,
    createdBy: json['createdBy'] as String,
    updatedAt: json['updatedAt'] as String,
    updatedBy: json['updatedBy'] as String,
    inspectionFormTemplateDetail: (json['inspectionFormTemplateDetail'] as List)
        ?.map((e) => e == null
            ? null
            : InspectionFormTemplateDetail.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    inspectionResponseDetail: (json['inspectionResponseDetail'] as List)
        ?.map((e) => e == null
            ? null
            : InspectionResponseDetail.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    inspectionTransactionDetail: (json['inspectionTransactionDetail'] as List)
        ?.map((e) => e == null
            ? null
            : InspectionTransactionDetail.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$InspectionResponseToJson(InspectionResponse instance) =>
    <String, dynamic>{
      'pk': instance.pk,
      'name': instance.name,
      'isDeleted': instance.isDeleted,
      'createdAt': instance.createdAt,
      'createdBy': instance.createdBy,
      'updatedAt': instance.updatedAt,
      'updatedBy': instance.updatedBy,
      'inspectionFormTemplateDetail': instance.inspectionFormTemplateDetail,
      'inspectionResponseDetail': instance.inspectionResponseDetail,
      'inspectionTransactionDetail': instance.inspectionTransactionDetail,
    };
