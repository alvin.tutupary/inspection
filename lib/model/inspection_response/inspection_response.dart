import 'package:inspection/model/inspection_form_template_detail/inspection_form_template_detail.dart';
import 'package:inspection/model/inspection_response_detail/inspection_response_detail.dart';
import 'package:inspection/model/inspection_transaction_detail/inspection_transaction_detail.dart';

import 'package:json_annotation/json_annotation.dart';
part 'inspection_response.g.dart';

@JsonSerializable()
class InspectionResponse {
  String pk;
  String name;
  bool isDeleted;
  String createdAt;
  String createdBy;
  String updatedAt;
  String updatedBy;

  List<InspectionFormTemplateDetail> inspectionFormTemplateDetail;
  List<InspectionResponseDetail> inspectionResponseDetail;
  List<InspectionTransactionDetail> inspectionTransactionDetail;

  InspectionResponse({
    this.pk,
    this.name,
    this.isDeleted,
    this.createdAt,
    this.createdBy,
    this.updatedAt,
    this.updatedBy,
    this.inspectionFormTemplateDetail,
    this.inspectionResponseDetail,
    this.inspectionTransactionDetail,
  });

  @override
  String toString() {
    return """
    InspectionResponse{
      pk: $pk,
      name: $name,
      isDeleted: $isDeleted,
      createdAt: $createdAt,
      createdBy: $createdBy,
      updatedAt: $updatedAt,
      updatedBy: $updatedBy,
      inspectionFormTemplateDetail: $inspectionFormTemplateDetail,
      inspectionResponseDetail: $inspectionResponseDetail,
      inspectionTransactionDetail: $inspectionTransactionDetail,
    }
    """;
  }

  factory InspectionResponse.fromJson(Map<String, dynamic> json) =>
      _$InspectionResponseFromJson(json);

  Map<String, dynamic> toJson() => _$InspectionResponseToJson(this);
}
