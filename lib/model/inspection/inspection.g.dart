// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'inspection.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CreateInspectionPIC _$CreateInspectionPICFromJson(Map<String, dynamic> json) {
  return CreateInspectionPIC(
    pkuser: json['pkuser'] as String,
    fullname: json['fullname'] as String,
  );
}

Map<String, dynamic> _$CreateInspectionPICToJson(
        CreateInspectionPIC instance) =>
    <String, dynamic>{
      'pkuser': instance.pkuser,
      'fullname': instance.fullname,
    };

UpdateInspectionPIC _$UpdateInspectionPICFromJson(Map<String, dynamic> json) {
  return UpdateInspectionPIC(
    pk: json['pk'] as String,
    pkuser: json['pkuser'] as String,
  );
}

Map<String, dynamic> _$UpdateInspectionPICToJson(
        UpdateInspectionPIC instance) =>
    <String, dynamic>{
      'pk': instance.pk,
      'pkuser': instance.pkuser,
    };

CreateInspectionFormTemplate _$CreateInspectionFormTemplateFromJson(
    Map<String, dynamic> json) {
  return CreateInspectionFormTemplate(
    name: json['name'] as String,
    ispublishtoapps: json['ispublishtoapps'] as bool,
    isresponsemandatory: json['isresponsemandatory'] as bool,
    headers: (json['headers'] as List)
        ?.map((e) => e == null
            ? null
            : CreateInspectionFormTemplateHeader.fromJson(
                e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$CreateInspectionFormTemplateToJson(
        CreateInspectionFormTemplate instance) =>
    <String, dynamic>{
      'name': instance.name,
      'ispublishtoapps': instance.ispublishtoapps,
      'isresponsemandatory': instance.isresponsemandatory,
      'headers': instance.headers,
    };

CreateInspectionFormTemplateHeader _$CreateInspectionFormTemplateHeaderFromJson(
    Map<String, dynamic> json) {
  return CreateInspectionFormTemplateHeader(
    header: json['header'] as String,
    seq: json['seq'] as int,
    details: (json['details'] as List)
        ?.map((e) => e == null
            ? null
            : CreateInspectionFormTemplateDetail.fromJson(
                e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$CreateInspectionFormTemplateHeaderToJson(
        CreateInspectionFormTemplateHeader instance) =>
    <String, dynamic>{
      'header': instance.header,
      'seq': instance.seq,
      'details': instance.details,
    };

CreateInspectionFormTemplateDetail _$CreateInspectionFormTemplateDetailFromJson(
    Map<String, dynamic> json) {
  return CreateInspectionFormTemplateDetail(
    question: json['question'] as String,
    model: json['model'] as String,
    seq: json['seq'] as int,
    seqheader: json['seqheader'] as int,
    ishasimage: json['ishasimage'] as bool,
  );
}

Map<String, dynamic> _$CreateInspectionFormTemplateDetailToJson(
        CreateInspectionFormTemplateDetail instance) =>
    <String, dynamic>{
      'question': instance.question,
      'model': instance.model,
      'seq': instance.seq,
      'seqheader': instance.seqheader,
      'ishasimage': instance.ishasimage,
    };

UpdateInspectionFormTemplate _$UpdateInspectionFormTemplateFromJson(
    Map<String, dynamic> json) {
  return UpdateInspectionFormTemplate(
    pk: json['pk'] as String,
    name: json['name'] as String,
    ispublishtoapps: json['ispublishtoapps'] as bool,
    isresponsemandatory: json['isresponsemandatory'] as bool,
    headers: (json['headers'] as List)
        ?.map((e) => e == null
            ? null
            : UpdateInspectionFormTemplateHeader.fromJson(
                e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$UpdateInspectionFormTemplateToJson(
        UpdateInspectionFormTemplate instance) =>
    <String, dynamic>{
      'pk': instance.pk,
      'name': instance.name,
      'ispublishtoapps': instance.ispublishtoapps,
      'isresponsemandatory': instance.isresponsemandatory,
      'headers': instance.headers,
    };

UpdateInspectionFormTemplateHeader _$UpdateInspectionFormTemplateHeaderFromJson(
    Map<String, dynamic> json) {
  return UpdateInspectionFormTemplateHeader(
    pk: json['pk'] as String,
    header: json['header'] as String,
    seq: json['seq'] as int,
    details: (json['details'] as List)
        ?.map((e) => e == null
            ? null
            : UpdateInspectionFormTemplateDetail.fromJson(
                e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$UpdateInspectionFormTemplateHeaderToJson(
        UpdateInspectionFormTemplateHeader instance) =>
    <String, dynamic>{
      'pk': instance.pk,
      'header': instance.header,
      'seq': instance.seq,
      'details': instance.details,
    };

UpdateInspectionFormTemplateDetail _$UpdateInspectionFormTemplateDetailFromJson(
    Map<String, dynamic> json) {
  return UpdateInspectionFormTemplateDetail(
    pk: json['pk'] as String,
    question: json['question'] as String,
    model: json['model'] as String,
    seq: json['seq'] as int,
    seqheader: json['seqheader'] as int,
    ishasimage: json['ishasimage'] as bool,
  );
}

Map<String, dynamic> _$UpdateInspectionFormTemplateDetailToJson(
        UpdateInspectionFormTemplateDetail instance) =>
    <String, dynamic>{
      'pk': instance.pk,
      'question': instance.question,
      'model': instance.model,
      'seq': instance.seq,
      'seqheader': instance.seqheader,
      'ishasimage': instance.ishasimage,
    };

CreateInspectionResponse _$CreateInspectionResponseFromJson(
    Map<String, dynamic> json) {
  return CreateInspectionResponse(
    name: json['name'] as String,
    details: (json['details'] as List)
        ?.map((e) => e == null
            ? null
            : CreateInspectionResponseDetail.fromJson(
                e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$CreateInspectionResponseToJson(
        CreateInspectionResponse instance) =>
    <String, dynamic>{
      'name': instance.name,
      'details': instance.details,
    };

CreateInspectionResponseDetail _$CreateInspectionResponseDetailFromJson(
    Map<String, dynamic> json) {
  return CreateInspectionResponseDetail(
    choicetext: json['choicetext'] as String,
    seq: json['seq'] as int,
    scorevalue: json['scorevalue'] as int,
    iscountdivider: json['iscountdivider'] as bool,
  );
}

Map<String, dynamic> _$CreateInspectionResponseDetailToJson(
        CreateInspectionResponseDetail instance) =>
    <String, dynamic>{
      'choicetext': instance.choicetext,
      'seq': instance.seq,
      'scorevalue': instance.scorevalue,
      'iscountdivider': instance.iscountdivider,
    };

UpdateInspectionResponse _$UpdateInspectionResponseFromJson(
    Map<String, dynamic> json) {
  return UpdateInspectionResponse(
    pk: json['pk'] as String,
    name: json['name'] as String,
    details: (json['details'] as List)
        ?.map((e) => e == null
            ? null
            : UpdateInspectionResponseDetail.fromJson(
                e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$UpdateInspectionResponseToJson(
        UpdateInspectionResponse instance) =>
    <String, dynamic>{
      'pk': instance.pk,
      'name': instance.name,
      'details': instance.details,
    };

UpdateInspectionResponseDetail _$UpdateInspectionResponseDetailFromJson(
    Map<String, dynamic> json) {
  return UpdateInspectionResponseDetail(
    pk: json['pk'] as String,
    choicetext: json['choicetext'] as String,
    seq: json['seq'] as int,
    scorevalue: json['scorevalue'] as int,
    iscountdivider: json['iscountdivider'] as bool,
  );
}

Map<String, dynamic> _$UpdateInspectionResponseDetailToJson(
        UpdateInspectionResponseDetail instance) =>
    <String, dynamic>{
      'pk': instance.pk,
      'choicetext': instance.choicetext,
      'seq': instance.seq,
      'scorevalue': instance.scorevalue,
      'iscountdivider': instance.iscountdivider,
    };

UpdateInspectionTransactionSubmit _$UpdateInspectionTransactionSubmitFromJson(
    Map<String, dynamic> json) {
  return UpdateInspectionTransactionSubmit(
    pk: json['pk'] as String,
    actualstartdate: json['actualstartdate'] as String,
    actualenddate: json['actualenddate'] as String,
    signature: json['signature'] as String,
    inspectorname: json['inspectorname'] as String,
    details: (json['details'] as List)
        ?.map((e) => e == null
            ? null
            : CreateInspectionTransactionDetail.fromJson(
                e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$UpdateInspectionTransactionSubmitToJson(
        UpdateInspectionTransactionSubmit instance) =>
    <String, dynamic>{
      'pk': instance.pk,
      'actualstartdate': instance.actualstartdate,
      'actualenddate': instance.actualenddate,
      'signature': instance.signature,
      'inspectorname': instance.inspectorname,
      'details': instance.details,
    };

CreateInspectionTransactionDetail _$CreateInspectionTransactionDetailFromJson(
    Map<String, dynamic> json) {
  return CreateInspectionTransactionDetail(
    pktemplateheader: json['pktemplateheader'] as String,
    pktemplatedetail: json['pktemplatedetail'] as String,
    pkresponse: json['pkresponse'] as String,
    pkresponsedetail: json['pkresponsedetail'] as String,
    model: json['model'] as String,
    response: json['response'] as String,
    responsecolor: json['responsecolor'] as String,
    description: json['description'] as String,
    solution: json['solution'] as String,
    result: json['result'] as String,
    images: (json['images'] as List)
        ?.map((e) =>
            e == null ? null : InspectionTransactionDetailImage.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$CreateInspectionTransactionDetailToJson(
        CreateInspectionTransactionDetail instance) =>
    <String, dynamic>{
      'pktemplateheader': instance.pktemplateheader,
      'pktemplatedetail': instance.pktemplatedetail,
      'pkresponse': instance.pkresponse,
      'pkresponsedetail': instance.pkresponsedetail,
      'model': instance.model,
      'response': instance.response,
      'responsecolor': instance.responsecolor,
      'description': instance.description,
      'solution': instance.solution,
      'result': instance.result,
      'images': instance.images,
    };

InspectionTransactionDetailImage _$InspectionTransactionDetailImageFromJson(
    Map<String, dynamic> json) {
  return InspectionTransactionDetailImage(
    filename: json['filename'] as String,
    mimetype: json['mimetype'] as String,
    base64string: json['base64string'] as String,
    ext: json['ext'] as String,
  );
}

Map<String, dynamic> _$InspectionTransactionDetailImageToJson(
        InspectionTransactionDetailImage instance) =>
    <String, dynamic>{
      'filename': instance.filename,
      'mimetype': instance.mimetype,
      'base64string': instance.base64string,
      'ext': instance.ext,
    };
