import 'package:json_annotation/json_annotation.dart';

part 'inspection.g.dart';

@JsonSerializable()
class CreateInspectionPIC {
  String pkuser;
  String fullname;

  CreateInspectionPIC({
    this.pkuser,
    this.fullname,
  });

  @override
  String toString() {
    return """
  CreateInspectionPIC{
    pkuser: $pkuser,
    fullname: $fullname
  }
  """;
  }

  factory CreateInspectionPIC.fromJson(Map<String, dynamic> json) =>
      _$CreateInspectionPICFromJson(json);

  Map<String, dynamic> toJson() => _$CreateInspectionPICToJson(this);
}

@JsonSerializable()
class UpdateInspectionPIC {
  String pk;
  String pkuser;

  UpdateInspectionPIC({
    this.pk,
    this.pkuser,
  });

  @override
  String toString() {
    return """
  UpdateInspectionPIC{
    pk: $pk,
    pkuser: $pkuser
  }
  """;
  }

  factory UpdateInspectionPIC.fromJson(Map<String, dynamic> json) =>
      _$UpdateInspectionPICFromJson(json);

  Map<String, dynamic> toJson() => _$UpdateInspectionPICToJson(this);
}

@JsonSerializable()
class CreateInspectionFormTemplate {
  String name;
  bool ispublishtoapps;
  bool isresponsemandatory;
  List<CreateInspectionFormTemplateHeader> headers;

  CreateInspectionFormTemplate({
    this.name,
    this.ispublishtoapps,
    this.isresponsemandatory,
    this.headers,
  });

  @override
  String toString() {
    return """
  CreateInspectionFormTemplate{
    name: $name,
    ispublishtoapps: $ispublishtoapps,
    isresponsemandatory: $isresponsemandatory,
    headers: $headers,
  }
  """;
  }

  factory CreateInspectionFormTemplate.fromJson(Map<String, dynamic> json) =>
      _$CreateInspectionFormTemplateFromJson(json);

  Map<String, dynamic> toJson() => _$CreateInspectionFormTemplateToJson(this);
}

@JsonSerializable()
class CreateInspectionFormTemplateHeader {
  String header;
  int seq;
  List<CreateInspectionFormTemplateDetail> details;

  CreateInspectionFormTemplateHeader({
    this.header,
    this.seq,
    this.details,
  });

  @override
  String toString() {
    return """
  CreateInspectionFormTemplateHeader{
    header: $header,
    seq: $seq,
    details: $details,
  }
  """;
  }

  factory CreateInspectionFormTemplateHeader.fromJson(
          Map<String, dynamic> json) =>
      _$CreateInspectionFormTemplateHeaderFromJson(json);

  Map<String, dynamic> toJson() =>
      _$CreateInspectionFormTemplateHeaderToJson(this);
}

@JsonSerializable()
class CreateInspectionFormTemplateDetail {
  String question;
  String model;
  int seq;
  int seqheader;
  bool ishasimage;

  CreateInspectionFormTemplateDetail({
    this.question,
    this.model,
    this.seq,
    this.seqheader,
    this.ishasimage,
  });

  @override
  String toString() {
    return """
  CreateInspectionFormTemplateDetail{
    question: $question,
    model: $model,
    seq: $seq,
    seqheader: $seqheader,
    ishasimage: $ishasimage,
  }
  """;
  }

  factory CreateInspectionFormTemplateDetail.fromJson(
          Map<String, dynamic> json) =>
      _$CreateInspectionFormTemplateDetailFromJson(json);

  Map<String, dynamic> toJson() =>
      _$CreateInspectionFormTemplateDetailToJson(this);
}

@JsonSerializable()
class UpdateInspectionFormTemplate {
  String pk;
  String name;
  bool ispublishtoapps;
  bool isresponsemandatory;
  List<UpdateInspectionFormTemplateHeader> headers;

  UpdateInspectionFormTemplate({
    this.pk,
    this.name,
    this.ispublishtoapps,
    this.isresponsemandatory,
    this.headers,
  });

  @override
  String toString() {
    return """
  UpdateInspectionFormTemplate{
    pk: $pk,
    name: $name,
    ispublishtoapps: $ispublishtoapps,
    isresponsemandatory: $isresponsemandatory,
    headers: $headers,
  }
  """;
  }

  factory UpdateInspectionFormTemplate.fromJson(Map<String, dynamic> json) =>
      _$UpdateInspectionFormTemplateFromJson(json);

  Map<String, dynamic> toJson() => _$UpdateInspectionFormTemplateToJson(this);
}

@JsonSerializable()
class UpdateInspectionFormTemplateHeader {
  String pk;
  String header;
  int seq;
  List<UpdateInspectionFormTemplateDetail> details;

  UpdateInspectionFormTemplateHeader({
    this.pk,
    this.header,
    this.seq,
    this.details,
  });

  @override
  String toString() {
    return """
  UpdateInspectionFormTemplateHeader{
    pk: $pk,
    header: $header,
    seq: $seq,
    details: $details,
  }
  """;
  }

  factory UpdateInspectionFormTemplateHeader.fromJson(
          Map<String, dynamic> json) =>
      _$UpdateInspectionFormTemplateHeaderFromJson(json);

  Map<String, dynamic> toJson() =>
      _$UpdateInspectionFormTemplateHeaderToJson(this);
}

@JsonSerializable()
class UpdateInspectionFormTemplateDetail {
  String pk;
  String question;
  String model;
  int seq;
  int seqheader;
  bool ishasimage;

  UpdateInspectionFormTemplateDetail({
    this.pk,
    this.question,
    this.model,
    this.seq,
    this.seqheader,
    this.ishasimage,
  });

  @override
  String toString() {
    return """
  UpdateInspectionFormTemplateDetail{
    pk: $pk,
    question: $question,
    model: $model,
    seq: $seq,
    seqheader: $seqheader,
    ishasimage: $ishasimage,
  }
  """;
  }

  factory UpdateInspectionFormTemplateDetail.fromJson(
          Map<String, dynamic> json) =>
      _$UpdateInspectionFormTemplateDetailFromJson(json);

  Map<String, dynamic> toJson() =>
      _$UpdateInspectionFormTemplateDetailToJson(this);
}

@JsonSerializable()
class CreateInspectionResponse {
  String name;
  List<CreateInspectionResponseDetail> details;

  CreateInspectionResponse({
    this.name,
    this.details,
  });

  @override
  String toString() {
    return """
  CreateInspectionResponse{
    name: $name,
    details: $details,
  }
  """;
  }

  factory CreateInspectionResponse.fromJson(Map<String, dynamic> json) =>
      _$CreateInspectionResponseFromJson(json);

  Map<String, dynamic> toJson() => _$CreateInspectionResponseToJson(this);
}

@JsonSerializable()
class CreateInspectionResponseDetail {
  String choicetext;
  int seq;
  int scorevalue;
  bool iscountdivider;

  CreateInspectionResponseDetail({
    this.choicetext,
    this.seq,
    this.scorevalue,
    this.iscountdivider,
  });

  @override
  String toString() {
    return """
  CreateInspectionResponseDetail{
    choicetext: $choicetext,
    seq: $seq,
    scorevalue: $scorevalue,
    iscountdivider: $iscountdivider,
  }
  """;
  }

  factory CreateInspectionResponseDetail.fromJson(Map<String, dynamic> json) =>
      _$CreateInspectionResponseDetailFromJson(json);

  Map<String, dynamic> toJson() => _$CreateInspectionResponseDetailToJson(this);
}

@JsonSerializable()
class UpdateInspectionResponse {
  String pk;
  String name;
  List<UpdateInspectionResponseDetail> details;

  UpdateInspectionResponse({
    this.pk,
    this.name,
    this.details,
  });

  @override
  String toString() {
    return """
  UpdateInspectionResponse{
    pk: $pk,
    name: $name,
    details: $details,
  }
  """;
  }

  factory UpdateInspectionResponse.fromJson(Map<String, dynamic> json) =>
      _$UpdateInspectionResponseFromJson(json);

  Map<String, dynamic> toJson() => _$UpdateInspectionResponseToJson(this);
}

@JsonSerializable()
class UpdateInspectionResponseDetail {
  String pk;
  String choicetext;
  int seq;
  int scorevalue;
  bool iscountdivider;

  UpdateInspectionResponseDetail({
    this.pk,
    this.choicetext,
    this.seq,
    this.scorevalue,
    this.iscountdivider,
  });

  @override
  String toString() {
    return """
  UpdateInspectionResponseDetail{
    pk: $pk,
    choicetext: $choicetext,
    seq: $seq,
    scorevalue: $scorevalue,
    iscountdivider: $iscountdivider,
  }
  """;
  }

  factory UpdateInspectionResponseDetail.fromJson(Map<String, dynamic> json) =>
      _$UpdateInspectionResponseDetailFromJson(json);

  Map<String, dynamic> toJson() => _$UpdateInspectionResponseDetailToJson(this);
}

@JsonSerializable()
class UpdateInspectionTransactionSubmit {
  String pk;
  String actualstartdate;
  String actualenddate;
  String signature;
  String inspectorname;
  List<CreateInspectionTransactionDetail> details;

  UpdateInspectionTransactionSubmit(
      {this.pk,
      this.actualstartdate,
      this.actualenddate,
      this.signature,
      this.inspectorname,
      this.details});

  @override
  String toString() {
    return """
  UpdateInspectionTransactionSubmit{
    pk: $pk,
    actualstartdate: $actualstartdate,
    actualenddate: $actualenddate,
    signature: $signature,
    inspectorname: $inspectorname,
    details: $details
  }
  """;
  }

  factory UpdateInspectionTransactionSubmit.fromJson(
          Map<String, dynamic> json) =>
      _$UpdateInspectionTransactionSubmitFromJson(json);

  Map<String, dynamic> toJson() =>
      _$UpdateInspectionTransactionSubmitToJson(this);
}

@JsonSerializable()
class CreateInspectionTransactionDetail {
  String pktemplateheader;
  String pktemplatedetail;
  String pkresponse;
  String pkresponsedetail;
  String model;
  String response;
  String responsecolor;
  String description;
  String solution;
  String result;
  List<InspectionTransactionDetailImage> images;

  CreateInspectionTransactionDetail({
    this.pktemplateheader,
    this.pktemplatedetail,
    this.pkresponse,
    this.pkresponsedetail,
    this.model,
    this.response,
    this.responsecolor,
    this.description,
    this.solution,
    this.result,
    this.images
  });

  @override
  String toString() {
    return """
  CreateInspectionTransactionDetail{
    pktemplateheader: $pktemplateheader,
    pktemplatedetail: $pktemplatedetail,
    pkresponse: $pkresponse,
    pkresponsedetail: $pkresponsedetail,
    model: $model,
    response: $response,
    responsecolor: $responsecolor,
    description: $description,
    solution: $solution,
    result: $result,
    images: $images,
  }
  """;
  }

  factory CreateInspectionTransactionDetail.fromJson(
          Map<String, dynamic> json) =>
      _$CreateInspectionTransactionDetailFromJson(json);

  Map<String, dynamic> toJson() =>
      _$CreateInspectionTransactionDetailToJson(this);
}

@JsonSerializable()
class InspectionTransactionDetailImage {
  String filename;
  String mimetype;
  String base64string;
  String ext;

  InspectionTransactionDetailImage({
    this.filename,
    this.mimetype,
    this.base64string,
    this.ext
  });

  @override
  String toString() {
    return """
  InspectionTransactionDetailImage{
    filename: $filename,
    mimetype: $mimetype,
    base64string: $base64string,
    ext: $ext
  }
  """;
  }

  factory InspectionTransactionDetailImage.fromJson(
          Map<String, dynamic> json) =>
      _$InspectionTransactionDetailImageFromJson(json);

  Map<String, dynamic> toJson() =>
      _$InspectionTransactionDetailImageToJson(this);
}