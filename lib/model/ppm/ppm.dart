import 'package:inspection/model/category/category.dart';
import 'package:inspection/model/floor/floor.dart';
import 'package:inspection/model/inspection_form_template/inspection_form_template.dart';
import 'package:inspection/model/inspection_pic/inspection_pic.dart';
import 'package:inspection/model/inspection_transaction/inspection_transaction.dart';
import 'package:inspection/model/room/room.dart';
import 'package:inspection/model/service_provider/service_provider.dart';

import 'package:json_annotation/json_annotation.dart';

part 'ppm.g.dart';

@JsonSerializable()
class PpmActivity {
  String pk;
  String pkFloor;
  String pkRoom;
  String pkMaintenanceCategory;
  String pkWorkCategory;
  String pkInspectionPic;
  String pkServiceProvider;
  String pkServiceProviderUser;
  String pkInspectionFormTemplate;
  String no;
  String location;
  String workTitle;
  String description;
  String starDateFirst;
  String endDateFirst;
  String endDate;
  String periodeType;
  String frequency;
  String status;
  int reminder;
  bool isSkipHoliday;
  String createdAt;
  String createdBy;
  String updatedAt;
  String updatedBy;

  Floor pkFloorNavigation;
  InspectionFormTemplate pkInspectionFormTemplateNavigation;
  InspectionPic pkInspectionPicNavigation;
  Category pkMaintenanceCategoryNavigation;
  Room pkRoomNavigation;
  ServiceProvider pkServiceProviderNavigation;
  ServiceProviderUser pkServiceProviderUserNavigation;
  Category pkWorkCategoryNavigation;
  List<InspectionTransaction> inspectionTransaction;
  List<PpmActivitySchedule> ppmActivitySchedule;

  PpmActivity({
    this.pk,
    this.pkFloor,
    this.pkRoom,
    this.pkMaintenanceCategory,
    this.pkWorkCategory,
    this.pkInspectionPic,
    this.pkServiceProvider,
    this.pkServiceProviderUser,
    this.pkInspectionFormTemplate,
    this.no,
    this.location,
    this.workTitle,
    this.description,
    this.starDateFirst,
    this.endDateFirst,
    this.endDate,
    this.periodeType,
    this.frequency,
    this.status,
    this.reminder,
    this.isSkipHoliday,
    this.createdAt,
    this.createdBy,
    this.updatedAt,
    this.updatedBy,
    this.pkFloorNavigation,
    this.pkInspectionFormTemplateNavigation,
    this.pkInspectionPicNavigation,
    this.pkMaintenanceCategoryNavigation,
    this.pkRoomNavigation,
    this.pkServiceProviderNavigation,
    this.pkServiceProviderUserNavigation,
    this.pkWorkCategoryNavigation,
    this.inspectionTransaction,
    this.ppmActivitySchedule,
  });

  @override
  String toString() {
    return """
    PpmActivity{
      pk: $pk,
      pkFloor: $pkFloor,
      pkRoom: $pkRoom,
      pkMaintenanceCategory: $pkMaintenanceCategory,
      pkWorkCategory: $pkWorkCategory,
      pkInspectionPic: $pkInspectionPic,
      pkServiceProvider: $pkServiceProvider,
      pkServiceProviderUser: $pkServiceProviderUser,
      pkInspectionFormTemplate: $pkInspectionFormTemplate,
      no: $no,
      location: $location,
      workTitle: $workTitle,
      description: $description,
      starDateFirst: $starDateFirst,
      endDateFirst: $endDateFirst,
      endDate: $endDate,
      periodeType: $periodeType,
      frequency: $frequency,
      status: $status,
      reminder: $reminder,
      isSkipHoliday: $isSkipHoliday,
      createdAt: $createdAt,
      createdBy: $createdBy,
      updatedAt: $updatedAt,
      updatedBy: $updatedBy,
      pkFloorNavigation: $pkFloorNavigation,
      pkInspectionFormTemplateNavigation: $pkInspectionFormTemplateNavigation,
      pkInspectionPicNavigation: $pkInspectionPicNavigation,
      pkMaintenanceCategoryNavigation: $pkMaintenanceCategoryNavigation,
      pkRoomNavigation: $pkRoomNavigation,
      pkServiceProviderNavigation: $pkServiceProviderNavigation,
      pkServiceProviderUserNavigation: $pkServiceProviderUserNavigation,
      pkWorkCategoryNavigation: $pkWorkCategoryNavigation,
      inspectionTransaction: $inspectionTransaction,
      ppmActivitySchedule: $ppmActivitySchedule,   
    }
    """;
  }

  factory PpmActivity.fromJson(Map<String, dynamic> json) =>
      _$PpmActivityFromJson(json);

  Map<String, dynamic> toJson() => _$PpmActivityToJson(this);
}

@JsonSerializable()
class PpmActivitySchedule {
  String pk;
  String pkPpmActivity;
  String pkInspectionPic;
  String pkInspectionFormTemplate;
  String location;
  String code;
  String planStartDate;
  String planEndDate;
  String actualStartDate;
  String actualEndDate;
  String status;
  String result;
  bool isJsaRequired;
  bool isServiceReportRequired;
  bool isInspectionReportRequired;
  String createdAt;
  String createdBy;
  String updatedAt;
  String updatedBy;

  InspectionFormTemplate pkInspectionFormTemplateNavigation;
  InspectionPic pkInspectionPicNavigation;
  PpmActivity pkPpmActivityNavigation;
  InspectionTransaction inspectionTransaction;

  PpmActivitySchedule({
    this.pk,
    this.pkPpmActivity,
    this.pkInspectionPic,
    this.pkInspectionFormTemplate,
    this.location,
    this.code,
    this.planStartDate,
    this.planEndDate,
    this.actualStartDate,
    this.actualEndDate,
    this.status,
    this.result,
    this.isJsaRequired,
    this.isServiceReportRequired,
    this.isInspectionReportRequired,
    this.createdAt,
    this.createdBy,
    this.updatedAt,
    this.updatedBy,
    this.pkInspectionFormTemplateNavigation,
    this.pkInspectionPicNavigation,
    this.pkPpmActivityNavigation,
    this.inspectionTransaction,
  });

  @override
  String toString() {
    return """
    PpmActivitySchedule{
      pk: $pk,
      pkPpmActivity: $pkPpmActivity,
      pkInspectionPic: $pkInspectionPic,
      pkInspectionFormTemplate: $pkInspectionFormTemplate,
      location: $location,
      code: $code,
      planStartDate: $planStartDate,
      planEndDate: $planEndDate,
      actualStartDate: $actualStartDate,
      actualEndDate: $actualEndDate,
      status: $status,
      result: $result,
      isJsaRequired: $isJsaRequired,
      isServiceReportRequired: $isServiceReportRequired,
      isInspectionReportRequired: $isInspectionReportRequired,
      createdAt: $createdAt,
      createdBy: $createdBy,
      updatedAt: $updatedAt,
      updatedBy: $updatedBy,
      pkInspectionFormTemplateNavigation: $pkInspectionFormTemplateNavigation,
      pkInspectionPicNavigation: $pkInspectionPicNavigation,
      pkPpmActivityNavigation: $pkPpmActivityNavigation,
      inspectionTransaction: $inspectionTransaction,
    }
    """;
  }

  factory PpmActivitySchedule.fromJson(Map<String, dynamic> json) =>
      _$PpmActivityScheduleFromJson(json);

  Map<String, dynamic> toJson() => _$PpmActivityScheduleToJson(this);
}
