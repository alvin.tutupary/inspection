// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ppm.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PpmActivity _$PpmActivityFromJson(Map<String, dynamic> json) {
  return PpmActivity(
    pk: json['pk'] as String,
    pkFloor: json['pkFloor'] as String,
    pkRoom: json['pkRoom'] as String,
    pkMaintenanceCategory: json['pkMaintenanceCategory'] as String,
    pkWorkCategory: json['pkWorkCategory'] as String,
    pkInspectionPic: json['pkInspectionPic'] as String,
    pkServiceProvider: json['pkServiceProvider'] as String,
    pkServiceProviderUser: json['pkServiceProviderUser'] as String,
    pkInspectionFormTemplate: json['pkInspectionFormTemplate'] as String,
    no: json['no'] as String,
    location: json['location'] as String,
    workTitle: json['workTitle'] as String,
    description: json['description'] as String,
    starDateFirst: json['starDateFirst'] as String,
    endDateFirst: json['endDateFirst'] as String,
    endDate: json['endDate'] as String,
    periodeType: json['periodeType'] as String,
    frequency: json['frequency'] as String,
    status: json['status'] as String,
    reminder: json['reminder'] as int,
    isSkipHoliday: json['isSkipHoliday'] as bool,
    createdAt: json['createdAt'] as String,
    createdBy: json['createdBy'] as String,
    updatedAt: json['updatedAt'] as String,
    updatedBy: json['updatedBy'] as String,
    pkFloorNavigation: json['pkFloorNavigation'] == null
        ? null
        : Floor.fromJson(json['pkFloorNavigation'] as Map<String, dynamic>),
    pkInspectionFormTemplateNavigation:
        json['pkInspectionFormTemplateNavigation'] == null
            ? null
            : InspectionFormTemplate.fromJson(
                json['pkInspectionFormTemplateNavigation']
                    as Map<String, dynamic>),
    pkInspectionPicNavigation: json['pkInspectionPicNavigation'] == null
        ? null
        : InspectionPic.fromJson(
            json['pkInspectionPicNavigation'] as Map<String, dynamic>),
    pkMaintenanceCategoryNavigation: json['pkMaintenanceCategoryNavigation'] ==
            null
        ? null
        : Category.fromJson(
            json['pkMaintenanceCategoryNavigation'] as Map<String, dynamic>),
    pkRoomNavigation: json['pkRoomNavigation'] == null
        ? null
        : Room.fromJson(json['pkRoomNavigation'] as Map<String, dynamic>),
    pkServiceProviderNavigation: json['pkServiceProviderNavigation'] == null
        ? null
        : ServiceProvider.fromJson(
            json['pkServiceProviderNavigation'] as Map<String, dynamic>),
    pkServiceProviderUserNavigation: json['pkServiceProviderUserNavigation'] ==
            null
        ? null
        : ServiceProviderUser.fromJson(
            json['pkServiceProviderUserNavigation'] as Map<String, dynamic>),
    pkWorkCategoryNavigation: json['pkWorkCategoryNavigation'] == null
        ? null
        : Category.fromJson(
            json['pkWorkCategoryNavigation'] as Map<String, dynamic>),
    inspectionTransaction: (json['inspectionTransaction'] as List)
        ?.map((e) => e == null
            ? null
            : InspectionTransaction.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    ppmActivitySchedule: (json['ppmActivitySchedule'] as List)
        ?.map((e) => e == null
            ? null
            : PpmActivitySchedule.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$PpmActivityToJson(PpmActivity instance) =>
    <String, dynamic>{
      'pk': instance.pk,
      'pkFloor': instance.pkFloor,
      'pkRoom': instance.pkRoom,
      'pkMaintenanceCategory': instance.pkMaintenanceCategory,
      'pkWorkCategory': instance.pkWorkCategory,
      'pkInspectionPic': instance.pkInspectionPic,
      'pkServiceProvider': instance.pkServiceProvider,
      'pkServiceProviderUser': instance.pkServiceProviderUser,
      'pkInspectionFormTemplate': instance.pkInspectionFormTemplate,
      'no': instance.no,
      'location': instance.location,
      'workTitle': instance.workTitle,
      'description': instance.description,
      'starDateFirst': instance.starDateFirst,
      'endDateFirst': instance.endDateFirst,
      'endDate': instance.endDate,
      'periodeType': instance.periodeType,
      'frequency': instance.frequency,
      'status': instance.status,
      'reminder': instance.reminder,
      'isSkipHoliday': instance.isSkipHoliday,
      'createdAt': instance.createdAt,
      'createdBy': instance.createdBy,
      'updatedAt': instance.updatedAt,
      'updatedBy': instance.updatedBy,
      'pkFloorNavigation': instance.pkFloorNavigation,
      'pkInspectionFormTemplateNavigation':
          instance.pkInspectionFormTemplateNavigation,
      'pkInspectionPicNavigation': instance.pkInspectionPicNavigation,
      'pkMaintenanceCategoryNavigation':
          instance.pkMaintenanceCategoryNavigation,
      'pkRoomNavigation': instance.pkRoomNavigation,
      'pkServiceProviderNavigation': instance.pkServiceProviderNavigation,
      'pkServiceProviderUserNavigation':
          instance.pkServiceProviderUserNavigation,
      'pkWorkCategoryNavigation': instance.pkWorkCategoryNavigation,
      'inspectionTransaction': instance.inspectionTransaction,
      'ppmActivitySchedule': instance.ppmActivitySchedule,
    };

PpmActivitySchedule _$PpmActivityScheduleFromJson(Map<String, dynamic> json) {
  return PpmActivitySchedule(
    pk: json['pk'] as String,
    pkPpmActivity: json['pkPpmActivity'] as String,
    pkInspectionPic: json['pkInspectionPic'] as String,
    pkInspectionFormTemplate: json['pkInspectionFormTemplate'] as String,
    location: json['location'] as String,
    code: json['code'] as String,
    planStartDate: json['planStartDate'] as String,
    planEndDate: json['planEndDate'] as String,
    actualStartDate: json['actualStartDate'] as String,
    actualEndDate: json['actualEndDate'] as String,
    status: json['status'] as String,
    result: json['result'] as String,
    isJsaRequired: json['isJsaRequired'] as bool,
    isServiceReportRequired: json['isServiceReportRequired'] as bool,
    isInspectionReportRequired: json['isInspectionReportRequired'] as bool,
    createdAt: json['createdAt'] as String,
    createdBy: json['createdBy'] as String,
    updatedAt: json['updatedAt'] as String,
    updatedBy: json['updatedBy'] as String,
    pkInspectionFormTemplateNavigation:
        json['pkInspectionFormTemplateNavigation'] == null
            ? null
            : InspectionFormTemplate.fromJson(
                json['pkInspectionFormTemplateNavigation']
                    as Map<String, dynamic>),
    pkInspectionPicNavigation: json['pkInspectionPicNavigation'] == null
        ? null
        : InspectionPic.fromJson(
            json['pkInspectionPicNavigation'] as Map<String, dynamic>),
    pkPpmActivityNavigation: json['pkPpmActivityNavigation'] == null
        ? null
        : PpmActivity.fromJson(
            json['pkPpmActivityNavigation'] as Map<String, dynamic>),
    inspectionTransaction: json['inspectionTransaction'] == null
        ? null
        : InspectionTransaction.fromJson(
            json['inspectionTransaction'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$PpmActivityScheduleToJson(
        PpmActivitySchedule instance) =>
    <String, dynamic>{
      'pk': instance.pk,
      'pkPpmActivity': instance.pkPpmActivity,
      'pkInspectionPic': instance.pkInspectionPic,
      'pkInspectionFormTemplate': instance.pkInspectionFormTemplate,
      'location': instance.location,
      'code': instance.code,
      'planStartDate': instance.planStartDate,
      'planEndDate': instance.planEndDate,
      'actualStartDate': instance.actualStartDate,
      'actualEndDate': instance.actualEndDate,
      'status': instance.status,
      'result': instance.result,
      'isJsaRequired': instance.isJsaRequired,
      'isServiceReportRequired': instance.isServiceReportRequired,
      'isInspectionReportRequired': instance.isInspectionReportRequired,
      'createdAt': instance.createdAt,
      'createdBy': instance.createdBy,
      'updatedAt': instance.updatedAt,
      'updatedBy': instance.updatedBy,
      'pkInspectionFormTemplateNavigation':
          instance.pkInspectionFormTemplateNavigation,
      'pkInspectionPicNavigation': instance.pkInspectionPicNavigation,
      'pkPpmActivityNavigation': instance.pkPpmActivityNavigation,
      'inspectionTransaction': instance.inspectionTransaction,
    };
