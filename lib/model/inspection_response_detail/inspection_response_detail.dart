import 'package:inspection/model/inspection_response/inspection_response.dart';
import 'package:inspection/model/inspection_transaction_detail/inspection_transaction_detail.dart';

import 'package:json_annotation/json_annotation.dart';
part 'inspection_response_detail.g.dart';

@JsonSerializable()
class InspectionResponseDetail {
  String pk;
  String pkInspectionResponse;
  String choiceText;
  int seq;
  int scoreValue;
  bool isCountDivider;
  bool isDeleted;
  String color;
  String createdAt;
  String createdBy;
  String updatedAt;
  String updatedBy;

  InspectionResponse pkInspectionResponseNavigation;
  List<InspectionTransactionDetail> inspectionTransactionDetail;

  InspectionResponseDetail(
      {this.pk,
      this.pkInspectionResponse,
      this.choiceText,
      this.seq,
      this.scoreValue,
      this.isCountDivider,
      this.isDeleted,
      this.color,
      this.createdAt,
      this.createdBy,
      this.updatedAt,
      this.updatedBy,
      this.pkInspectionResponseNavigation,
      this.inspectionTransactionDetail});

  @override
  String toString() {
    return """
    InspectionResponseDetail{
      pk: $pk,
      pkInspectionResponse: $pkInspectionResponse,
      choiceText: $choiceText,
      seq: $seq,
      scoreValue: $scoreValue,
      isCountDivider: $isCountDivider,
      isDeleted: $isDeleted,
      color: $color,
      createdAt: $createdAt,
      createdBy: $createdBy,
      updatedAt: $updatedAt,
      updatedBy: $updatedBy,
      pkInspectionResponseNavigation: $pkInspectionResponseNavigation,
      inspectionTransactionDetail: $inspectionTransactionDetail
    }
    """;
  }

  factory InspectionResponseDetail.fromJson(Map<String, dynamic> json) =>
      _$InspectionResponseDetailFromJson(json);

  Map<String, dynamic> toJson() => _$InspectionResponseDetailToJson(this);
}
