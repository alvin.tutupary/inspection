// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'inspection_response_detail.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

InspectionResponseDetail _$InspectionResponseDetailFromJson(
    Map<String, dynamic> json) {
  return InspectionResponseDetail(
    pk: json['pk'] as String,
    pkInspectionResponse: json['pkInspectionResponse'] as String,
    choiceText: json['choiceText'] as String,
    seq: json['seq'] as int,
    scoreValue: json['scoreValue'] as int,
    isCountDivider: json['isCountDivider'] as bool,
    isDeleted: json['isDeleted'] as bool,
    color: json['color'] as String,
    createdAt: json['createdAt'] as String,
    createdBy: json['createdBy'] as String,
    updatedAt: json['updatedAt'] as String,
    updatedBy: json['updatedBy'] as String,
    pkInspectionResponseNavigation:
        json['pkInspectionResponseNavigation'] == null
            ? null
            : InspectionResponse.fromJson(
                json['pkInspectionResponseNavigation'] as Map<String, dynamic>),
    inspectionTransactionDetail: (json['inspectionTransactionDetail'] as List)
        ?.map((e) => e == null
            ? null
            : InspectionTransactionDetail.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$InspectionResponseDetailToJson(
        InspectionResponseDetail instance) =>
    <String, dynamic>{
      'pk': instance.pk,
      'pkInspectionResponse': instance.pkInspectionResponse,
      'choiceText': instance.choiceText,
      'seq': instance.seq,
      'scoreValue': instance.scoreValue,
      'isCountDivider': instance.isCountDivider,
      'isDeleted': instance.isDeleted,
      'color': instance.color,
      'createdAt': instance.createdAt,
      'createdBy': instance.createdBy,
      'updatedAt': instance.updatedAt,
      'updatedBy': instance.updatedBy,
      'pkInspectionResponseNavigation': instance.pkInspectionResponseNavigation,
      'inspectionTransactionDetail': instance.inspectionTransactionDetail,
    };
