// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'inspection_form_template.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

InspectionFormTemplate _$InspectionFormTemplateFromJson(
    Map<String, dynamic> json) {
  return InspectionFormTemplate(
    pk: json['pk'] as String,
    name: json['name'] as String,
    isPublishToApps: json['isPublishToApps'] as bool,
    isResponseMandatory: json['isResponseMandatory'] as bool,
    isDeleted: json['isDeleted'] as bool,
    createdAt: json['createdAt'] as String,
    createdBy: json['createdBy'] as String,
    updatedAt: json['updatedAt'] as String,
    updatedBy: json['updatedBy'] as String,
    inspectionFormTemplateHeader: (json['inspectionFormTemplateHeader'] as List)
        ?.map((e) => e == null
            ? null
            : InspectionFormTemplateHeader.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    inspectionTransaction: (json['inspectionTransaction'] as List)
        ?.map((e) => e == null
            ? null
            : InspectionTransaction.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    ppmActivity: (json['ppmActivity'] as List)
        ?.map((e) =>
            e == null ? null : PpmActivity.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    ppmActivitySchedule: (json['ppmActivitySchedule'] as List)
        ?.map((e) => e == null
            ? null
            : PpmActivitySchedule.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$InspectionFormTemplateToJson(
        InspectionFormTemplate instance) =>
    <String, dynamic>{
      'pk': instance.pk,
      'name': instance.name,
      'isPublishToApps': instance.isPublishToApps,
      'isResponseMandatory': instance.isResponseMandatory,
      'isDeleted': instance.isDeleted,
      'createdAt': instance.createdAt,
      'createdBy': instance.createdBy,
      'updatedAt': instance.updatedAt,
      'updatedBy': instance.updatedBy,
      'inspectionFormTemplateHeader': instance.inspectionFormTemplateHeader,
      'inspectionTransaction': instance.inspectionTransaction,
      'ppmActivity': instance.ppmActivity,
      'ppmActivitySchedule': instance.ppmActivitySchedule,
    };
