import 'package:inspection/model/inspection_form_template_header/inspection_form_template_header.dart';
import 'package:inspection/model/inspection_transaction/inspection_transaction.dart';
import 'package:inspection/model/ppm/ppm.dart';
import 'package:json_annotation/json_annotation.dart';
part 'inspection_form_template.g.dart';

@JsonSerializable()
class InspectionFormTemplate {
  String pk;
  String name;
  bool isPublishToApps;
  bool isResponseMandatory;
  bool isDeleted;
  String createdAt;
  String createdBy;
  String updatedAt;
  String updatedBy;

  List<InspectionFormTemplateHeader> inspectionFormTemplateHeader;
  List<InspectionTransaction> inspectionTransaction;
  List<PpmActivity> ppmActivity;
  List<PpmActivitySchedule> ppmActivitySchedule;

  InspectionFormTemplate({
    this.pk,
    this.name,
    this.isPublishToApps,
    this.isResponseMandatory,
    this.isDeleted,
    this.createdAt,
    this.createdBy,
    this.updatedAt,
    this.updatedBy,
    this.inspectionFormTemplateHeader,
    this.inspectionTransaction,
    this.ppmActivity,
    this.ppmActivitySchedule,
  });

  @override
  String toString() {
    return """
    InspectionFormTemplate{
      pk: $pk,
      name: $name,
      isPublishToApps: $isPublishToApps,
      isResponseMandatory: $isResponseMandatory,
      isDeleted: $isDeleted,
      createdAt: $createdAt,
      createdBy: $createdBy,
      updatedAt: $updatedAt,
      updatedBy: $updatedBy,
      inspectionFormTemplateHeader: $inspectionFormTemplateHeader,
      inspectionTransaction: $inspectionTransaction,
      ppmActivity: $ppmActivity,
      ppmActivitySchedule: $ppmActivitySchedule,
    }
    """;
  }

  factory InspectionFormTemplate.fromJson(Map<String, dynamic> json) =>
      _$InspectionFormTemplateFromJson(json);

  Map<String, dynamic> toJson() => _$InspectionFormTemplateToJson(this);
}
