// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'inspection_pic.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

InspectionPic _$InspectionPicFromJson(Map<String, dynamic> json) {
  return InspectionPic(
    records: json['records'] as int,
    pk: json['pk'] as String,
    pkUser: json['pkUser'] as String,
    fullname: json['fullname'] as String,
    isActive: json['isActive'] as bool,
    isDeleted: json['isDeleted'] as bool,
    createdAt: json['createdAt'] as String,
    createdBy: json['createdBy'] as String,
    updatedAt: json['updatedAt'] as String,
    updatedBy: json['updatedBy'] as String,
    inspectionTransaction: (json['inspectionTransaction'] as List)
        ?.map((e) => e == null
            ? null
            : InspectionTransaction.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    ppmActivity: (json['ppmActivity'] as List)
        ?.map((e) =>
            e == null ? null : PpmActivity.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    ppmActivitySchedule: (json['ppmActivitySchedule'] as List)
        ?.map((e) => e == null
            ? null
            : PpmActivitySchedule.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$InspectionPicToJson(InspectionPic instance) =>
    <String, dynamic>{
      'records': instance.records,
      'pk': instance.pk,
      'pkUser': instance.pkUser,
      'fullname': instance.fullname,
      'isActive': instance.isActive,
      'isDeleted': instance.isDeleted,
      'createdAt': instance.createdAt,
      'createdBy': instance.createdBy,
      'updatedAt': instance.updatedAt,
      'updatedBy': instance.updatedBy,
      'inspectionTransaction': instance.inspectionTransaction,
      'ppmActivity': instance.ppmActivity,
      'ppmActivitySchedule': instance.ppmActivitySchedule,
    };
