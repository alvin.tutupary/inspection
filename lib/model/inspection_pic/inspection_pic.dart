import 'package:inspection/model/inspection_transaction/inspection_transaction.dart';
import 'package:inspection/model/ppm/ppm.dart';

import 'package:json_annotation/json_annotation.dart';
part 'inspection_pic.g.dart';

@JsonSerializable()
class InspectionPic {
  int records;
  String pk;
  String pkUser;
  String fullname;
  bool isActive;
  bool isDeleted;
  String createdAt;
  String createdBy;
  String updatedAt;
  String updatedBy;

  List<InspectionTransaction> inspectionTransaction;
  List<PpmActivity> ppmActivity;
  List<PpmActivitySchedule> ppmActivitySchedule;

  InspectionPic({
    this.records,
    this.pk,
    this.pkUser,
    this.fullname,
    this.isActive,
    this.isDeleted,
    this.createdAt,
    this.createdBy,
    this.updatedAt,
    this.updatedBy,
    this.inspectionTransaction,
    this.ppmActivity,
    this.ppmActivitySchedule,
  });

  @override
  String toString() {
    return """
    InspectionPic{
      records : $records,
      pk : $pk,
      pkUser : $pkUser,
      fullname : $fullname,
      isActive : $isActive,
      isDeleted : $isDeleted,
      createdAt : $createdAt,
      createdBy : $createdBy,
      updatedAt : $updatedAt,
      updatedBy : $updatedBy,
      inspectionTransaction : $inspectionTransaction,
      ppmActivity : $ppmActivity,
      ppmActivitySchedule : $ppmActivitySchedule
    }
    """;
  }

  factory InspectionPic.fromJson(Map<String, dynamic> json) =>
      _$InspectionPicFromJson(json);

  Map<String, dynamic> toJson() => _$InspectionPicToJson(this);
}
