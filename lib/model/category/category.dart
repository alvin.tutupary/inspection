import 'package:inspection/model/ppm/ppm.dart';
import 'package:json_annotation/json_annotation.dart';

part 'category.g.dart';

@JsonSerializable()
class Category {
  String pk;
  String pkparent;
  String pkgroup;
  String sla;
  String srt;
  String categoryType;
  String priority;
  String name;
  String workflowId;
  String directWorker;
  bool isDeleted;
  String createdAt;
  String createdBy;
  String updatedAt;
  String updatedBy;

  List<Category> inversePkparentNavigation;
  List<PpmActivity> ppmActivityPkMaintenanceCategoryNavigation;
  List<PpmActivity> ppmActivityPkWorkCategoryNavigation;

  Category({
    this.pk,
    this.pkparent,
    this.pkgroup,
    this.sla,
    this.srt,
    this.categoryType,
    this.priority,
    this.name,
    this.workflowId,
    this.directWorker,
    this.isDeleted,
    this.createdAt,
    this.createdBy,
    this.updatedAt,
    this.updatedBy,
    this.inversePkparentNavigation,
    this.ppmActivityPkMaintenanceCategoryNavigation,
    this.ppmActivityPkWorkCategoryNavigation,
  });

  @override
  String toString() {
    return """
    Category{
      pk: $pk,
      pkparent: $pkparent,
      pkgroup: $pkgroup,
      sla: $sla,
      srt: $srt,
      categoryType: $categoryType,
      priority: $priority,
      name: $name,
      workflowId: $workflowId,
      directWorker: $directWorker,
      isDeleted: $isDeleted,
      createdAt: $createdAt,
      createdBy: $createdBy,
      updatedAt: $updatedAt,
      updatedBy: $updatedBy,
      inversePkparentNavigation: $inversePkparentNavigation,
      ppmActivityPkMaintenanceCategoryNavigation: $ppmActivityPkMaintenanceCategoryNavigation,
      ppmActivityPkWorkCategoryNavigation: $ppmActivityPkWorkCategoryNavigation,
    }
    """;
  }

  factory Category.fromJson(Map<String, dynamic> json) =>
      _$CategoryFromJson(json);

  Map<String, dynamic> toJson() => _$CategoryToJson(this);
}
