// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'category.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Category _$CategoryFromJson(Map<String, dynamic> json) {
  return Category(
    pk: json['pk'] as String,
    pkparent: json['pkparent'] as String,
    pkgroup: json['pkgroup'] as String,
    sla: json['sla'] as String,
    srt: json['srt'] as String,
    categoryType: json['categoryType'] as String,
    priority: json['priority'] as String,
    name: json['name'] as String,
    workflowId: json['workflowId'] as String,
    directWorker: json['directWorker'] as String,
    isDeleted: json['isDeleted'] as bool,
    createdAt: json['createdAt'] as String,
    createdBy: json['createdBy'] as String,
    updatedAt: json['updatedAt'] as String,
    updatedBy: json['updatedBy'] as String,
    inversePkparentNavigation: (json['inversePkparentNavigation'] as List)
        ?.map((e) =>
            e == null ? null : Category.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    ppmActivityPkMaintenanceCategoryNavigation:
        (json['ppmActivityPkMaintenanceCategoryNavigation'] as List)
            ?.map((e) => e == null
                ? null
                : PpmActivity.fromJson(e as Map<String, dynamic>))
            ?.toList(),
    ppmActivityPkWorkCategoryNavigation:
        (json['ppmActivityPkWorkCategoryNavigation'] as List)
            ?.map((e) => e == null
                ? null
                : PpmActivity.fromJson(e as Map<String, dynamic>))
            ?.toList(),
  );
}

Map<String, dynamic> _$CategoryToJson(Category instance) => <String, dynamic>{
      'pk': instance.pk,
      'pkparent': instance.pkparent,
      'pkgroup': instance.pkgroup,
      'sla': instance.sla,
      'srt': instance.srt,
      'categoryType': instance.categoryType,
      'priority': instance.priority,
      'name': instance.name,
      'workflowId': instance.workflowId,
      'directWorker': instance.directWorker,
      'isDeleted': instance.isDeleted,
      'createdAt': instance.createdAt,
      'createdBy': instance.createdBy,
      'updatedAt': instance.updatedAt,
      'updatedBy': instance.updatedBy,
      'inversePkparentNavigation': instance.inversePkparentNavigation,
      'ppmActivityPkMaintenanceCategoryNavigation':
          instance.ppmActivityPkMaintenanceCategoryNavigation,
      'ppmActivityPkWorkCategoryNavigation':
          instance.ppmActivityPkWorkCategoryNavigation,
    };
