// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'inspection_form_template_header.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

InspectionFormTemplateHeader _$InspectionFormTemplateHeaderFromJson(
    Map<String, dynamic> json) {
  return InspectionFormTemplateHeader(
    pk: json['pk'] as String,
    pkInspectionFormTemplate: json['pkInspectionFormTemplate'] as String,
    header: json['header'] as String,
    seq: json['seq'] as int,
    isDeleted: json['isDeleted'] as bool,
    createdAt: json['createdAt'] == null
        ? null
        : DateTime.parse(json['createdAt'] as String),
    createdBy: json['createdBy'] as String,
    updatedAt: json['updatedAt'] == null
        ? null
        : DateTime.parse(json['updatedAt'] as String),
    updatedBy: json['updatedBy'] as String,
    pkInspectionFormTemplateNavigation:
        json['pkInspectionFormTemplateNavigation'] == null
            ? null
            : InspectionFormTemplate.fromJson(
                json['pkInspectionFormTemplateNavigation']
                    as Map<String, dynamic>),
    inspectionFormTemplateDetail: (json['inspectionFormTemplateDetail'] as List)
        ?.map((e) => e == null
            ? null
            : InspectionFormTemplateDetail.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    inspectionTransactionDetail: (json['inspectionTransactionDetail'] as List)
        ?.map((e) => e == null
            ? null
            : InspectionTransactionDetail.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$InspectionFormTemplateHeaderToJson(
        InspectionFormTemplateHeader instance) =>
    <String, dynamic>{
      'pk': instance.pk,
      'pkInspectionFormTemplate': instance.pkInspectionFormTemplate,
      'header': instance.header,
      'seq': instance.seq,
      'isDeleted': instance.isDeleted,
      'createdAt': instance.createdAt?.toIso8601String(),
      'createdBy': instance.createdBy,
      'updatedAt': instance.updatedAt?.toIso8601String(),
      'updatedBy': instance.updatedBy,
      'pkInspectionFormTemplateNavigation':
          instance.pkInspectionFormTemplateNavigation,
      'inspectionFormTemplateDetail': instance.inspectionFormTemplateDetail,
      'inspectionTransactionDetail': instance.inspectionTransactionDetail,
    };
