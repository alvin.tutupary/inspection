import 'package:inspection/model/inspection_form_template/inspection_form_template.dart';
import 'package:inspection/model/inspection_form_template_detail/inspection_form_template_detail.dart';
import 'package:inspection/model/inspection_transaction_detail/inspection_transaction_detail.dart';
import 'package:json_annotation/json_annotation.dart';
part 'inspection_form_template_header.g.dart';

@JsonSerializable()
class InspectionFormTemplateHeader {
  String pk;
  String pkInspectionFormTemplate;
  String header;
  int seq;
  bool isDeleted;
  DateTime createdAt;
  String createdBy;
  DateTime updatedAt;
  String updatedBy;

  InspectionFormTemplate pkInspectionFormTemplateNavigation;
  List<InspectionFormTemplateDetail> inspectionFormTemplateDetail;
  List<InspectionTransactionDetail> inspectionTransactionDetail;

  InspectionFormTemplateHeader({
    this.pk,
    this.pkInspectionFormTemplate,
    this.header,
    this.seq,
    this.isDeleted,
    this.createdAt,
    this.createdBy,
    this.updatedAt,
    this.updatedBy,
    this.pkInspectionFormTemplateNavigation,
    this.inspectionFormTemplateDetail,
    this.inspectionTransactionDetail,
  });

  @override
  String toString() {
    return """
    InspectionFormTemplateHeader{
      pk: $pk,
      pkInspectionFormTemplate: $pkInspectionFormTemplate,
      header: $header,
      seq: $seq,
      isDeleted: $isDeleted,
      createdAt: $createdAt,
      createdBy: $createdBy,
      updatedAt: $updatedAt,
      updatedBy: $updatedBy,
      pkInspectionFormTemplateNavigation: $pkInspectionFormTemplateNavigation,
      inspectionFormTemplateDetail: $inspectionFormTemplateDetail,
      inspectionTransactionDetail: $inspectionTransactionDetail,
    }
    """;
  }

  factory InspectionFormTemplateHeader.fromJson(Map<String, dynamic> json) =>
      _$InspectionFormTemplateHeaderFromJson(json);

  Map<String, dynamic> toJson() => _$InspectionFormTemplateHeaderToJson(this);
}
