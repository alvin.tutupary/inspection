// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'inspection_transaction.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

InspectionTransaction _$InspectionTransactionFromJson(
    Map<String, dynamic> json) {
  return InspectionTransaction(
    records: json['records'] as int,
    pk: json['pk'] as String,
    pkPpmActivity: json['pkPpmActivity'] as String,
    pkInspectionFormTemplate: json['pkInspectionFormTemplate'] as String,
    pkInspectionPic: json['pkInspectionPic'] as String,
    inspectionDate: json['inspectionDate'] as String,
    submitedDate: json['submitedDate'] as String,
    issuedDate: json['issuedDate'] as String,
    isIssued: json['isIssued'] as bool,
    issuedBy: json['issuedBy'] as String,
    issuedByName: json['issuedByName'] as String,
    reviewDate: json['reviewDate'] as String,
    isReview: json['isReview'] as bool,
    reviewBy: json['reviewBy'] as String,
    reviewByName: json['reviewByName'] as String,
    status: json['status'] as String,
    result: json['result'] as String,
    createdAt: json['createdAt'] as String,
    createdBy: json['createdBy'] as String,
    updatedAt: json['updatedAt'] as String,
    updatedBy: json['updatedBy'] as String,
    pkInspectionFormTemplateNavigation:
        json['pkInspectionFormTemplateNavigation'] == null
            ? null
            : InspectionFormTemplate.fromJson(
                json['pkInspectionFormTemplateNavigation']
                    as Map<String, dynamic>),
    pkInspectionPicNavigation: json['pkInspectionPicNavigation'] == null
        ? null
        : InspectionPic.fromJson(
            json['pkInspectionPicNavigation'] as Map<String, dynamic>),
    pkNavigation: json['pkNavigation'] == null
        ? null
        : PpmActivitySchedule.fromJson(
            json['pkNavigation'] as Map<String, dynamic>),
    pkPpmActivityNavigation: json['pkPpmActivityNavigation'] == null
        ? null
        : PpmActivity.fromJson(
            json['pkPpmActivityNavigation'] as Map<String, dynamic>),
    inspectionTransactionDetail: (json['inspectionTransactionDetail'] as List)
        ?.map((e) => e == null
            ? null
            : InspectionTransactionDetail.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$InspectionTransactionToJson(
        InspectionTransaction instance) =>
    <String, dynamic>{
      'records': instance.records,
      'pk': instance.pk,
      'pkPpmActivity': instance.pkPpmActivity,
      'pkInspectionFormTemplate': instance.pkInspectionFormTemplate,
      'pkInspectionPic': instance.pkInspectionPic,
      'inspectionDate': instance.inspectionDate,
      'submitedDate': instance.submitedDate,
      'issuedDate': instance.issuedDate,
      'isIssued': instance.isIssued,
      'issuedBy': instance.issuedBy,
      'issuedByName': instance.issuedByName,
      'reviewDate': instance.reviewDate,
      'isReview': instance.isReview,
      'reviewBy': instance.reviewBy,
      'reviewByName': instance.reviewByName,
      'status': instance.status,
      'result': instance.result,
      'createdAt': instance.createdAt,
      'createdBy': instance.createdBy,
      'updatedAt': instance.updatedAt,
      'updatedBy': instance.updatedBy,
      'pkInspectionFormTemplateNavigation':
          instance.pkInspectionFormTemplateNavigation,
      'pkInspectionPicNavigation': instance.pkInspectionPicNavigation,
      'pkNavigation': instance.pkNavigation,
      'pkPpmActivityNavigation': instance.pkPpmActivityNavigation,
      'inspectionTransactionDetail': instance.inspectionTransactionDetail,
    };
