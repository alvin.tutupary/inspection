import 'package:inspection/model/inspection_form_template/inspection_form_template.dart';
import 'package:inspection/model/inspection_pic/inspection_pic.dart';
import 'package:inspection/model/inspection_transaction_detail/inspection_transaction_detail.dart';
import 'package:inspection/model/ppm/ppm.dart';

import 'package:json_annotation/json_annotation.dart';
part 'inspection_transaction.g.dart';

@JsonSerializable()
class InspectionTransaction {
  int records;
  String pk;
  String pkPpmActivity;
  String pkInspectionFormTemplate;
  String pkInspectionPic;
  String inspectionDate;
  String submitedDate;
  String issuedDate;
  bool isIssued;
  String issuedBy;
  String issuedByName;
  String reviewDate;
  bool isReview;
  String reviewBy;
  String reviewByName;
  String status;
  String result;
  String createdAt;
  String createdBy;
  String updatedAt;
  String updatedBy;

  InspectionFormTemplate pkInspectionFormTemplateNavigation;
  InspectionPic pkInspectionPicNavigation;
  PpmActivitySchedule pkNavigation;
  PpmActivity pkPpmActivityNavigation;
  List<InspectionTransactionDetail> inspectionTransactionDetail;

  String signature;
  String inspectorName;

  InspectionTransaction({
    this.records,
    this.pk,
    this.pkPpmActivity,
    this.pkInspectionFormTemplate,
    this.pkInspectionPic,
    this.inspectionDate,
    this.submitedDate,
    this.issuedDate,
    this.isIssued,
    this.issuedBy,
    this.issuedByName,
    this.reviewDate,
    this.isReview,
    this.reviewBy,
    this.reviewByName,
    this.status,
    this.result,
    this.createdAt,
    this.createdBy,
    this.updatedAt,
    this.updatedBy,
    this.pkInspectionFormTemplateNavigation,
    this.pkInspectionPicNavigation,
    this.pkNavigation,
    this.pkPpmActivityNavigation,
    this.inspectionTransactionDetail,
    this.signature,
    this.inspectorName
  });

  @override
  String toString() {
    return """
    InspectionTransaction{
      records: $records,
      pk: $pk,
      pkPpmActivity: $pkPpmActivity,
      pkInspectionFormTemplate: $pkInspectionFormTemplate,
      pkInspectionPic: $pkInspectionPic,
      inspectionDate: $inspectionDate,
      submitedDate: $submitedDate,
      issuedDate: $issuedDate,
      isIssued: $isIssued,
      issuedBy: $issuedBy,
      issuedByName: $issuedByName,
      reviewDate: $reviewDate,
      isReview: $isReview,
      reviewBy: $reviewBy,
      reviewByName: $reviewByName,
      status: $status,
      result: $result,
      createdAt: $createdAt,
      createdBy: $createdBy,
      updatedAt: $updatedAt,
      updatedBy: $updatedBy,

      pkInspectionFormTemplateNavigation: $pkInspectionFormTemplateNavigation,
      pkInspectionPicNavigation: $pkInspectionPicNavigation,
      pkNavigation: $pkNavigation,
      pkPpmActivityNavigation: $pkPpmActivityNavigation,
      inspectionTransactionDetail: $inspectionTransactionDetail,
    }
    """;
  }

  factory InspectionTransaction.fromJson(Map<String, dynamic> json) =>
      _$InspectionTransactionFromJson(json);

  Map<String, dynamic> toJson() => _$InspectionTransactionToJson(this);
}
