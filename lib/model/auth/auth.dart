import 'package:json_annotation/json_annotation.dart';
part 'auth.g.dart';

@JsonSerializable()
class Auth {
  String token;
  String refreshToken;

  Auth({
    this.token,
    this.refreshToken,
  });

  @override
  String toString() {
    return """
    Auth{
      token: $token,
      refreshToken: $refreshToken
    }
    """;
  }

  factory Auth.fromJson(Map<String, dynamic> json) => _$AuthFromJson(json);

  Map<String, dynamic> toJson() => _$AuthToJson(this);
}

@JsonSerializable()
class Login {
  String username;
  String password;
  String clientType;
  String appVersion;

  Login({this.username, this.password, this.clientType, this.appVersion});

  @override
  String toString() {
    return """
    Login{
      username: $username,
      password: $password,
      clientType: $clientType,
      appVersion: $appVersion
    }
    """;
  }

  Map<String, dynamic> toJson() => _$LoginToJson(this);
}

@JsonSerializable()
class Principal {
  @JsonKey(name: "nameid")
  String userId;
  @JsonKey(name: "Instance")
  String instance;
  @JsonKey(name: "NameInstance")
  String nameInstance;
  @JsonKey(name: "DivisiCode")
  String divisiCode;
  @JsonKey(name: "DivisiName")
  String divisiName;
  @JsonKey(name: "DepartmentCode")
  String departmentCode;
  @JsonKey(name: "DepartmentName")
  String departmentName;
  @JsonKey(name: "unique_name")
  String username;
  @JsonKey(name: "FullName")
  String fullName;
  @JsonKey(name: "Phone")
  String phone;
  @JsonKey(name: "email")
  String email;
  @JsonKey(name: "role")
  List<String> roles;
  @JsonKey(name: "Site")
  String site;
  @JsonKey(name: "NIK")
  String nik;
  @JsonKey(name: "IsLeader")
  bool isleader;
  @JsonKey(name: "IsAdmin")
  bool isadmin;
  @JsonKey(name: "IsMember")
  bool ismember;
  @JsonKey(name: "IsInspector")
  bool isinspector;
  @JsonKey(name: "iat")
  int tokenCreated;
  @JsonKey(name: "exp")
  int tokenExpired;

  Principal(
      {this.userId,
      this.instance,
      this.nameInstance,
      this.divisiCode,
      this.divisiName,
      this.departmentCode,
      this.departmentName,
      this.username,
      this.fullName,
      this.phone,
      this.email,
      this.roles,
      this.site,
      this.nik,
      this.isleader,
      this.isadmin,
      this.ismember,
      this.isinspector,
      this.tokenCreated,
      this.tokenExpired});

  @override
  String toString() {
    return """
    Principal{
      "userId": $userId,
      "instance": $instance,
      "nameInstance": $nameInstance,
      "divisiCode": $divisiCode,
      "divisiName": $divisiName,
      "departmentCode": $departmentCode,
      "departmentName": $departmentName,
      "username": $username,
      "fullName": $fullName,
      "phone": $phone,
      "email": $email,
      "roles": $roles,
      "site": $site,
      "nik": $nik,
      "isleader": $isleader,
      "isadmin": $isadmin,
      "ismember": $ismember,
      "isinspector": $isinspector,
      "tokenCreated": $tokenCreated,
      "tokenExpired": $tokenExpired,
    }
    """;
  }

  factory Principal.fromJson(Map<String, dynamic> json) =>
      _$PrincipalFromJson(json);

  Map<String, dynamic> toJson() => _$PrincipalToJson(this);
}
