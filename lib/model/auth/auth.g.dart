// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'auth.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Auth _$AuthFromJson(Map<String, dynamic> json) {
  return Auth(
    token: json['token'] as String,
    refreshToken: json['refreshToken'] as String,
  );
}

Map<String, dynamic> _$AuthToJson(Auth instance) => <String, dynamic>{
      'token': instance.token,
      'refreshToken': instance.refreshToken,
    };



Map<String, dynamic> _$LoginToJson(Login instance) => <String, dynamic>{
      'username': instance.username,
      'password': instance.password,
      'clientType': instance.clientType,
      'appVersion': instance.appVersion,
    };

Principal _$PrincipalFromJson(Map<String, dynamic> json) {
  return Principal(
    userId: json['nameid'] as String,
    instance: json['Instance'] as String,
    nameInstance: json['NameInstance'] as String,
    divisiCode: json['DivisiCode'] as String,
    divisiName: json['DivisiName'] as String,
    departmentCode: json['DepartmentCode'] as String,
    departmentName: json['DepartmentName'] as String,
    username: json['unique_name'] as String,
    fullName: json['FullName'] as String,
    phone: json['Phone'] as String,
    email: json['email'] as String,
    // roles: (json['role'] as List)?.map((e) => e as String)?.toList(),
    site: json['Site'] as String,
    nik: json['NIK'] as String,
    isleader: json['IsLeader'] == 'True' ? true : false,
    isadmin: json['IsAdmin'] == 'True' ? true : false,
    ismember: json['IsMember'] == 'True' ? true : false,
    isinspector: json['IsInspector'] == 'True' ? true : false,
    tokenCreated: json['iat'] as int,
    tokenExpired: json['exp'] as int,
  );
}

Map<String, dynamic> _$PrincipalToJson(Principal instance) => <String, dynamic>{
      'nameid': instance.userId,
      'Instance': instance.instance,
      'NameInstance': instance.nameInstance,
      'DivisiCode': instance.divisiCode,
      'DivisiName': instance.divisiName,
      'DepartmentCode': instance.departmentCode,
      'DepartmentName': instance.departmentName,
      'unique_name': instance.username,
      'FullName': instance.fullName,
      'Phone': instance.phone,
      'email': instance.email,
      'role': instance.roles,
      'Site': instance.site,
      'NIK': instance.nik,
      'IsLeader': instance.isleader,
      'IsAdmin': instance.isadmin,
      'IsMember': instance.ismember,
      'IsInspector': instance.isinspector,
      'iat': instance.tokenCreated,
      'exp': instance.tokenExpired,
    };
