import 'package:inspection/model/ppm/ppm.dart';
import 'package:inspection/model/room/room.dart';

import 'package:json_annotation/json_annotation.dart';

part 'floor.g.dart';

@JsonSerializable()
class Floor {
  String pk;
  String name;
  String location;
  bool isDeleted;
  String createdAt;
  String createdBy;
  String updatedAt;
  String updatedBy;

  List<PpmActivity> ppmActivity;
  List<Room> room;

  Floor({
    this.pk,
    this.name,
    this.location,
    this.isDeleted,
    this.createdAt,
    this.createdBy,
    this.updatedAt,
    this.updatedBy,
    this.ppmActivity,
    this.room,
  });

  @override
  String toString() {
    return """
    Floor{
      pk: $pk,
      name: $name,
      location: $location,
      isDeleted: $isDeleted,
      createdAt: $createdAt,
      createdBy: $createdBy,
      updatedAt: $updatedAt,
      updatedBy: $updatedBy,
      ppmActivity: $ppmActivity,
      room: $room,
    }
    """;
  }

  factory Floor.fromJson(Map<String, dynamic> json) => _$FloorFromJson(json);

  Map<String, dynamic> toJson() => _$FloorToJson(this);
}
