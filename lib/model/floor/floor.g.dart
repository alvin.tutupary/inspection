// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'floor.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Floor _$FloorFromJson(Map<String, dynamic> json) {
  return Floor(
    pk: json['pk'] as String,
    name: json['name'] as String,
    location: json['location'] as String,
    isDeleted: json['isDeleted'] as bool,
    createdAt: json['createdAt'] as String,
    createdBy: json['createdBy'] as String,
    updatedAt: json['updatedAt'] as String,
    updatedBy: json['updatedBy'] as String,
    ppmActivity: (json['ppmActivity'] as List)
        ?.map((e) =>
            e == null ? null : PpmActivity.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    room: (json['room'] as List)
        ?.map(
            (e) => e == null ? null : Room.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$FloorToJson(Floor instance) => <String, dynamic>{
      'pk': instance.pk,
      'name': instance.name,
      'location': instance.location,
      'isDeleted': instance.isDeleted,
      'createdAt': instance.createdAt,
      'createdBy': instance.createdBy,
      'updatedAt': instance.updatedAt,
      'updatedBy': instance.updatedBy,
      'ppmActivity': instance.ppmActivity,
      'room': instance.room,
    };
