import 'package:inspection/model/ppm/ppm.dart';
import 'package:json_annotation/json_annotation.dart';

part 'service_provider.g.dart';

@JsonSerializable()
class ServiceProvider {
  String pk;
  String defaultPic;
  String name;
  String address;
  String phone;
  bool isDeleted;
  String createdAt;
  String createdBy;
  String updatedAt;
  String updatedBy;

  List<PpmActivity> ppmActivity;
  List<ServiceProviderUser> serviceProviderUser;

  ServiceProvider({
    this.pk,
    this.defaultPic,
    this.name,
    this.address,
    this.phone,
    this.isDeleted,
    this.createdAt,
    this.createdBy,
    this.updatedAt,
    this.updatedBy,
    this.ppmActivity,
    this.serviceProviderUser,
  });

  @override
  String toString() {
    return """
    ServiceProvider{
      pk: $pk,
      defaultPic: $defaultPic,
      name: $name,
      address: $address,
      phone: $phone,
      isDeleted: $isDeleted,
      createdAt: $createdAt,
      createdBy: $createdBy,
      updatedAt: $updatedAt,
      updatedBy: $updatedBy,
      ppmActivity: $ppmActivity,
      serviceProviderUser: $serviceProviderUser,
    }
    """;
  }

  factory ServiceProvider.fromJson(Map<String, dynamic> json) =>
      _$ServiceProviderFromJson(json);

  Map<String, dynamic> toJson() => _$ServiceProviderToJson(this);
}

@JsonSerializable()
class ServiceProviderUser {
  String pk;
  String pkServiceProvider;
  String pkUser;
  String name;
  String email;
  String address;
  String phone;
  bool isDeleted;
  String createdAt;
  String createdBy;
  String updatedAt;
  String updatedBy;

  ServiceProvider pkServiceProviderNavigation;
  List<PpmActivity> ppmActivity;

  ServiceProviderUser({
    this.pk,
    this.pkServiceProvider,
    this.pkUser,
    this.name,
    this.email,
    this.address,
    this.phone,
    this.isDeleted,
    this.createdAt,
    this.createdBy,
    this.updatedAt,
    this.updatedBy,
    this.pkServiceProviderNavigation,
    this.ppmActivity,
  });

  @override
  String toString() {
    return """
    ServiceProviderUser{
pk: $pk,
pkServiceProvider: $pkServiceProvider,
pkUser: $pkUser,
name: $name,
email: $email,
address: $address,
phone: $phone,
isDeleted: $isDeleted,
createdAt: $createdAt,
createdBy: $createdBy,
updatedAt: $updatedAt,
updatedBy: $updatedBy,
pkServiceProviderNavigation: $pkServiceProviderNavigation,
ppmActivity: $ppmActivity,
    }
    """;
  }

  factory ServiceProviderUser.fromJson(Map<String, dynamic> json) =>
      _$ServiceProviderUserFromJson(json);

  Map<String, dynamic> toJson() => _$ServiceProviderUserToJson(this);
}
