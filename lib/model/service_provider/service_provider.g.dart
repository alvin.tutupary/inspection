// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'service_provider.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ServiceProvider _$ServiceProviderFromJson(Map<String, dynamic> json) {
  return ServiceProvider(
    pk: json['pk'] as String,
    defaultPic: json['defaultPic'] as String,
    name: json['name'] as String,
    address: json['address'] as String,
    phone: json['phone'] as String,
    isDeleted: json['isDeleted'] as bool,
    createdAt: json['createdAt'] as String,
    createdBy: json['createdBy'] as String,
    updatedAt: json['updatedAt'] as String,
    updatedBy: json['updatedBy'] as String,
    ppmActivity: (json['ppmActivity'] as List)
        ?.map((e) =>
            e == null ? null : PpmActivity.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    serviceProviderUser: (json['serviceProviderUser'] as List)
        ?.map((e) => e == null
            ? null
            : ServiceProviderUser.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$ServiceProviderToJson(ServiceProvider instance) =>
    <String, dynamic>{
      'pk': instance.pk,
      'defaultPic': instance.defaultPic,
      'name': instance.name,
      'address': instance.address,
      'phone': instance.phone,
      'isDeleted': instance.isDeleted,
      'createdAt': instance.createdAt,
      'createdBy': instance.createdBy,
      'updatedAt': instance.updatedAt,
      'updatedBy': instance.updatedBy,
      'ppmActivity': instance.ppmActivity,
      'serviceProviderUser': instance.serviceProviderUser,
    };

ServiceProviderUser _$ServiceProviderUserFromJson(Map<String, dynamic> json) {
  return ServiceProviderUser(
    pk: json['pk'] as String,
    pkServiceProvider: json['pkServiceProvider'] as String,
    pkUser: json['pkUser'] as String,
    name: json['name'] as String,
    email: json['email'] as String,
    address: json['address'] as String,
    phone: json['phone'] as String,
    isDeleted: json['isDeleted'] as bool,
    createdAt: json['createdAt'] as String,
    createdBy: json['createdBy'] as String,
    updatedAt: json['updatedAt'] as String,
    updatedBy: json['updatedBy'] as String,
    pkServiceProviderNavigation: json['pkServiceProviderNavigation'] == null
        ? null
        : ServiceProvider.fromJson(
            json['pkServiceProviderNavigation'] as Map<String, dynamic>),
    ppmActivity: (json['ppmActivity'] as List)
        ?.map((e) =>
            e == null ? null : PpmActivity.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$ServiceProviderUserToJson(
        ServiceProviderUser instance) =>
    <String, dynamic>{
      'pk': instance.pk,
      'pkServiceProvider': instance.pkServiceProvider,
      'pkUser': instance.pkUser,
      'name': instance.name,
      'email': instance.email,
      'address': instance.address,
      'phone': instance.phone,
      'isDeleted': instance.isDeleted,
      'createdAt': instance.createdAt,
      'createdBy': instance.createdBy,
      'updatedAt': instance.updatedAt,
      'updatedBy': instance.updatedBy,
      'pkServiceProviderNavigation': instance.pkServiceProviderNavigation,
      'ppmActivity': instance.ppmActivity,
    };
