import 'package:inspection/model/inspection_form_template_detail/inspection_form_template_detail.dart';
import 'package:inspection/model/inspection_form_template_header/inspection_form_template_header.dart';
import 'package:inspection/model/inspection_response/inspection_response.dart';
import 'package:inspection/model/inspection_response_detail/inspection_response_detail.dart';
import 'package:inspection/model/inspection_transaction/inspection_transaction.dart';

import 'package:json_annotation/json_annotation.dart';
part 'inspection_transaction_detail.g.dart';

@JsonSerializable()
class InspectionTransactionDetail {
  String pk;
  String pkInspectionTransaction;
  String pkInspectionFormTemplateHeader;
  String pkInspectionFormTemplateDetail;
  String pkInspectionResponse;
  String pkInspectionResponseDetail;
  String response;
  String color;
  String description;
  String solution;
  String filename;
  String mimetype;
  String ext;
  String result;
  String createdAt;
  String createdBy;
  String updatedAt;
  String updatedBy;

  InspectionFormTemplateDetail pkInspectionFormTemplateDetailNavigation;
  InspectionFormTemplateHeader pkInspectionFormTemplateHeaderNavigation;
  InspectionResponseDetail pkInspectionResponseDetailNavigation;
  InspectionResponse pkInspectionResponseNavigation;
  InspectionTransaction pkInspectionTransactionNavigation;

  InspectionTransactionDetail({
    this.pk,
    this.pkInspectionTransaction,
    this.pkInspectionFormTemplateHeader,
    this.pkInspectionFormTemplateDetail,
    this.pkInspectionResponse,
    this.pkInspectionResponseDetail,
    this.response,
    this.color,
    this.description,
    this.solution,
    this.filename,
    this.mimetype,
    this.ext,
    this.result,
    this.createdAt,
    this.createdBy,
    this.updatedAt,
    this.updatedBy,
    this.pkInspectionFormTemplateDetailNavigation,
    this.pkInspectionFormTemplateHeaderNavigation,
    this.pkInspectionResponseDetailNavigation,
    this.pkInspectionResponseNavigation,
    this.pkInspectionTransactionNavigation,
  });

  @override
  String toString() {
    return """
    InspectionTransactionDetail{
      pk: $pk,
      pkInspectionTransaction: $pkInspectionTransaction,
      pkInspectionFormTemplateHeader: $pkInspectionFormTemplateHeader,
      pkInspectionFormTemplateDetail: $pkInspectionFormTemplateDetail,
      pkInspectionResponse: $pkInspectionResponse,
      pkInspectionResponseDetail: $pkInspectionResponseDetail,
      response: $response,
      color: $color,
      description: $description,
      solution: $solution,
      filename: $filename,
      mimetype: $mimetype,
      ext: $ext,
      result: $result,
      createdAt: $createdAt,
      createdBy: $createdBy,
      updatedAt: $updatedAt,
      updatedBy: $updatedBy,
      pkInspectionFormTemplateDetailNavigation: $pkInspectionFormTemplateDetailNavigation,
      pkInspectionFormTemplateHeaderNavigation: $pkInspectionFormTemplateHeaderNavigation,
      pkInspectionResponseDetailNavigation: $pkInspectionResponseDetailNavigation,
      pkInspectionResponseNavigation: $pkInspectionResponseNavigation,
      pkInspectionTransactionNavigation: $pkInspectionTransactionNavigation,
    }
    """;
  }

  factory InspectionTransactionDetail.fromJson(Map<String, dynamic> json) =>
      _$InspectionTransactionDetailFromJson(json);

  Map<String, dynamic> toJson() => _$InspectionTransactionDetailToJson(this);
}
