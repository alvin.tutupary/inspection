// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'inspection_transaction_detail.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

InspectionTransactionDetail _$InspectionTransactionDetailFromJson(
    Map<String, dynamic> json) {
  return InspectionTransactionDetail(
    pk: json['pk'] as String,
    pkInspectionTransaction: json['pkInspectionTransaction'] as String,
    pkInspectionFormTemplateHeader:
        json['pkInspectionFormTemplateHeader'] as String,
    pkInspectionFormTemplateDetail:
        json['pkInspectionFormTemplateDetail'] as String,
    pkInspectionResponse: json['pkInspectionResponse'] as String,
    pkInspectionResponseDetail: json['pkInspectionResponseDetail'] as String,
    response: json['response'] as String,
    color: json['color'] as String,
    description: json['description'] as String,
    solution: json['solution'] as String,
    filename: json['filename'] as String,
    mimetype: json['mimetype'] as String,
    ext: json['ext'] as String,
    result: json['result'] as String,
    createdAt: json['createdAt'] as String,
    createdBy: json['createdBy'] as String,
    updatedAt: json['updatedAt'] as String,
    updatedBy: json['updatedBy'] as String,
    pkInspectionFormTemplateDetailNavigation:
        json['pkInspectionFormTemplateDetailNavigation'] == null
            ? null
            : InspectionFormTemplateDetail.fromJson(
                json['pkInspectionFormTemplateDetailNavigation']
                    as Map<String, dynamic>),
    pkInspectionFormTemplateHeaderNavigation:
        json['pkInspectionFormTemplateHeaderNavigation'] == null
            ? null
            : InspectionFormTemplateHeader.fromJson(
                json['pkInspectionFormTemplateHeaderNavigation']
                    as Map<String, dynamic>),
    pkInspectionResponseDetailNavigation:
        json['pkInspectionResponseDetailNavigation'] == null
            ? null
            : InspectionResponseDetail.fromJson(
                json['pkInspectionResponseDetailNavigation']
                    as Map<String, dynamic>),
    pkInspectionResponseNavigation:
        json['pkInspectionResponseNavigation'] == null
            ? null
            : InspectionResponse.fromJson(
                json['pkInspectionResponseNavigation'] as Map<String, dynamic>),
    pkInspectionTransactionNavigation:
        json['pkInspectionTransactionNavigation'] == null
            ? null
            : InspectionTransaction.fromJson(
                json['pkInspectionTransactionNavigation']
                    as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$InspectionTransactionDetailToJson(
        InspectionTransactionDetail instance) =>
    <String, dynamic>{
      'pk': instance.pk,
      'pkInspectionTransaction': instance.pkInspectionTransaction,
      'pkInspectionFormTemplateHeader': instance.pkInspectionFormTemplateHeader,
      'pkInspectionFormTemplateDetail': instance.pkInspectionFormTemplateDetail,
      'pkInspectionResponse': instance.pkInspectionResponse,
      'pkInspectionResponseDetail': instance.pkInspectionResponseDetail,
      'response': instance.response,
      'color': instance.color,
      'description': instance.description,
      'solution': instance.solution,
      'filename': instance.filename,
      'mimetype': instance.mimetype,
      'ext': instance.ext,
      'result': instance.result,
      'createdAt': instance.createdAt,
      'createdBy': instance.createdBy,
      'updatedAt': instance.updatedAt,
      'updatedBy': instance.updatedBy,
      'pkInspectionFormTemplateDetailNavigation':
          instance.pkInspectionFormTemplateDetailNavigation,
      'pkInspectionFormTemplateHeaderNavigation':
          instance.pkInspectionFormTemplateHeaderNavigation,
      'pkInspectionResponseDetailNavigation':
          instance.pkInspectionResponseDetailNavigation,
      'pkInspectionResponseNavigation': instance.pkInspectionResponseNavigation,
      'pkInspectionTransactionNavigation':
          instance.pkInspectionTransactionNavigation,
    };
