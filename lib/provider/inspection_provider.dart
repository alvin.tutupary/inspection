import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:inspection/helper/global.dart';
import 'package:inspection/model/inspection/inspection.dart';
import 'package:inspection/model/inspection_form_template_detail/inspection_form_template_detail.dart';
import 'package:inspection/model/inspection_form_template_header/inspection_form_template_header.dart';
import 'package:inspection/model/inspection_response_detail/inspection_response_detail.dart';
import 'package:inspection/model/inspection_transaction/inspection_transaction.dart';
import 'package:inspection/services/repositories.dart';
import 'package:path/path.dart';

class InspectionProvider with ChangeNotifier {
  bool _isLoading = false;
  InspectionTransaction _inspection;
  List<InspectionFormTemplateHeader> _inspectionFormTemplateHeaders = [];

  bool get isLoading => _isLoading;

  InspectionTransaction get inspection => _inspection;

  List<InspectionFormTemplateHeader> get inspectionFormTemplateHeaders {
    _inspectionFormTemplateHeaders = _inspection
        .pkInspectionFormTemplateNavigation.inspectionFormTemplateHeader;
    return [..._inspectionFormTemplateHeaders];
  }

  set isLoading(bool val){
    _isLoading = val;
    notifyListeners();
  }

  set inspection(InspectionTransaction value) {
    _inspection = value;
    notifyListeners();
  }

  void setDescription(InspectionFormTemplateDetail detail, String value) {
    var update = _inspectionFormTemplateHeaders
        .firstWhere((h) => h.pk == detail.pkInspectionFormTemplateHeader)
        .inspectionFormTemplateDetail
        .firstWhere((d) => d.pk == detail.pk);
    if (update != null) update.description = value;

    notifyListeners();
  }

  void setSolution(InspectionFormTemplateDetail detail, String value) {
    var update = _inspectionFormTemplateHeaders
        .firstWhere((h) => h.pk == detail.pkInspectionFormTemplateHeader)
        .inspectionFormTemplateDetail
        .firstWhere((d) => d.pk == detail.pk);
    if (update != null) update.solution = value;

    notifyListeners();
  }

  void setResponse(InspectionFormTemplateDetail detail,
      InspectionResponseDetail value, String text) {
    var update = _inspectionFormTemplateHeaders
        .firstWhere((h) => h.pk == detail.pkInspectionFormTemplateHeader)
        .inspectionFormTemplateDetail
        .firstWhere((d) => d.pk == detail.pk);
    if (update != null) {
      update.model == 'Text'
          ? update.responseText = text
          : update.response = value;
    }

    notifyListeners();
  }

  void setImage(InspectionFormTemplateDetail detail, String path) {
    var update = _inspectionFormTemplateHeaders
        .firstWhere((h) => h.pk == detail.pkInspectionFormTemplateHeader)
        .inspectionFormTemplateDetail
        .firstWhere((d) => d.pk == detail.pk);
    if (update != null) {
      final file = File(path);
      final bytes = file.readAsBytesSync();

      String base64string = base64Encode(bytes);
      if (update.images == null) update.images = [];

      update.images?.add(InspectionTransactionDetailImage(
          filename: basename(path),
          mimetype: "image/jpeg",
          base64string: base64string,
          ext: ".jpeg"));
    }
    notifyListeners();
  }

  Future<InspectionTransaction> getInspectionTransactionByPk(String pk) async {
    try {
      var _result = await Repository().getInspectionTransactionByPk(pk);
      return _result;
    } catch (e) {
      throw e;
    }
  }

  void reset() {}

  Future putInspectionTransactionSubmit() async {
    String message = '';
    bool tempRes;
    var createSubmit = new UpdateInspectionTransactionSubmit();
    try {
      createSubmit.pk = _inspection.pk;
      createSubmit.actualstartdate = appData.submitActualStartDate;
      createSubmit.actualenddate = DateTime.now().toString();
      createSubmit.signature = _inspection.signature;
      createSubmit.inspectorname = _inspection.inspectorName;
      createSubmit.details = [];


      int iter = 1;
      for (var header in _inspection
          .pkInspectionFormTemplateNavigation.inspectionFormTemplateHeader) {
        tempRes = true;
        if (tempRes) {
          for (var detail in header.inspectionFormTemplateDetail) {
            if (detail.model == 'Choice' && detail.response == null) {
              tempRes = false;
              message = message + "($iter) response is required \n";
            }
            if (detail.model == 'Text' && (detail.responseText == null || detail.responseText == '')) {
              tempRes = false;
              message = message + "($iter) response is required \n";
            }
            if (detail.model == 'Choice' && detail.response != null && detail.response.choiceText == 'Not Ok' ) {
              if (detail.description == null || detail.description == ''){
                tempRes = false;
                message = message + "($iter) description is required \n";
              }
              if (detail.images == null || detail.images == ''){
                tempRes = false;
                message = message + "($iter) images is required \n";
              }
              if(detail.solution == null || detail.solution == ''){
                tempRes = false;
                message = message + "($iter) solution is required \n";
              }
            }
            iter++;
            if (tempRes) {
              createSubmit.details.add(CreateInspectionTransactionDetail(
                  description: detail.description,
                  images: detail.images,
                  pkresponse: detail.pkInspectionResponse,
                  pkresponsedetail: detail.response?.pk,
                  pktemplatedetail: detail.pk,
                  pktemplateheader: detail.pkInspectionFormTemplateHeader,
                  response: detail.model == 'Text'
                      ? detail.responseText
                      : detail.response?.choiceText,
                  result: '',
                  model: detail.model,
                  responsecolor: detail.response?.color,
                  solution: detail.solution));
            }
            //  else {
            //   break;
            // }
          }
        } 
        // else {
        //   if (message == null || message.isEmpty) message = 'failed to submit';
          
        //   break;
        // }
      }

      if (message != null && message!= '')
        return message;
      else
        await Repository().putInspectionTransactionSubmit(createSubmit);

      notifyListeners();
    } catch (e) {
      throw e;
    }
  }
}

class ResponseApi {
  bool result;
  String message;
  ResponseApi(this.result, this.message);
}
