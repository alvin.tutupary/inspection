import 'package:flutter/cupertino.dart';
import 'package:inspection/model/inspection_transaction/inspection_transaction.dart';
import 'package:inspection/services/repositories.dart';

class InspectionsProvider with ChangeNotifier {
  List<InspectionTransaction> _inspections;
  int _page;
  int _max;
  int _maxPage;

  List<InspectionTransaction> get inspections {
    return [..._inspections];
  }

  set inspections(List<InspectionTransaction> value) {
    _inspections = value;
    notifyListeners();
  }

  int get page => _page;
  set setPage(int val) {
    _page = val;
  }

  int get max => _max;
  set setMax(int val) {
    _max = val;
  }

  int get maxPage => _maxPage;
  set setMaxPage(int val) {
    _maxPage = val;
  }

  InspectionTransaction findById(String pk) {
    return _inspections.firstWhere((p) => p.pk == pk);
  }

  Future<List<InspectionTransaction>> getInspectionTransactions() async {
    try {
      var _result = await Repository().getInspectionTransactions(
          true, null, true, 'Issued', '', '', page, max);
      return _result;
    } catch (e) {
      throw e;
    }
  }
}
