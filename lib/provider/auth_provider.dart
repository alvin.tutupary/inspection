import 'package:inspection/provider/base_provider.dart';
import 'package:inspection/services/repositories.dart';

class AuthProvider extends BaseProvider {
  String errMsg = '';
  bool isNavigate = false;

  Future doLogin(String userName, String password, String clientType,
      String version) async {
    setBusy(true);
    try {
      checkForm(userName, password);
      await Repository().doLogin(userName, password, clientType, version);
      isNavigate = true;
    } catch (ex) {
      errMsg = 'Wrong PIN / Password';
    }
    setBusy(false);
  }

  Future doRefresh() async {
    await Repository().doRefresh();
  }

  void checkForm(String userName, String password) async {
    try {
      if (userName.isEmpty) {
        errMsg = 'Check your username';
      } else if (password.isEmpty) {
        errMsg = 'Check your password';
      }
    } catch (e) {
      errMsg = 'Check Connection';
    }
  }
}
