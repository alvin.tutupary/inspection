import 'dart:convert';

import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
import 'package:inspection/helper/appSettings.dart';
import 'package:inspection/helper/clientApp.dart';
import 'package:inspection/helper/global.dart';
import 'package:inspection/model/inspection/inspection.dart';
import 'package:inspection/model/inspection_transaction/inspection_transaction.dart';

class InspectionService {
  Dio api = new ClientApi().dio;

  Future checkConnection() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.none) {
      throw Exception('No Internet Connection');
    }
  }

  Future<List<InspectionTransaction>> getInspectionTransactions(
      bool ispic,
      bool isadmin,
      bool issued,
      String status,
      String search,
      String orderby,
      int page,
      int max) async {
    try {
      await checkConnection();
      List<InspectionTransaction> resultDecode =
          new List<InspectionTransaction>();
      appData.isLogout = false;

      final result = await api.get(
          '${app.baseAddress}inspection/transaction/gets?ispic=$ispic&issued=$issued&status=$status&page=$page&max=$max');
      if (result.statusCode == 200) {
        final parsed = jsonDecode(result.data).cast<Map<String, dynamic>>();
        resultDecode = parsed
            .map<InspectionTransaction>(
                (json) => InspectionTransaction.fromJson(json))
            .toList();
      }
      return resultDecode;
    } catch (e) {
      throw e;
    }
  }

  Future<InspectionTransaction> getInspectionTransactionByPk(String pk) async {
    try {
      await checkConnection();
      InspectionTransaction resultDecode = new InspectionTransaction();

      appData.isLogout = false;

      final result =
          await api.get('${app.baseAddress}inspection/transaction/get/new/$pk');
      if (result.statusCode == 200) {
        final parsed = jsonDecode(result.data);
        resultDecode = InspectionTransaction.fromJson(parsed);
      }
      return resultDecode;
    } catch (e) {
      throw e;
    }
  }

  Future putInspectionTransactionSubmit(
      UpdateInspectionTransactionSubmit model) async {
    try {
      await checkConnection();
      await api.put('${app.baseAddress}inspection/transaction/update/submit',
          data: model);
    } catch (e) {
      throw e;
    }
  }
}
