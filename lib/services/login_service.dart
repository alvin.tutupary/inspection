import 'dart:convert';

import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:inspection/helper/appSettings.dart';
import 'package:inspection/helper/clientApp.dart';
import 'package:inspection/helper/global.dart';
import 'package:inspection/model/auth/auth.dart';

class AuthService {
  Dio api = new ClientApi().dio;
  Dio dioRefresh = new Dio(BaseOptions(
    baseUrl: app.baseAddress,
    // connectTimeout: 60000,
    // receiveTimeout: 90000,
    responseType: ResponseType.plain,
  ));
  final _storage = FlutterSecureStorage();

  Future<Auth> doLogin(String username, String password, String clientType,
      String version) async {
    try {
      await checkConnection();
      Response result = await api.post('${app.baseAddress}auth/login',
          data: Login(
              username: username,
              password: password,
              clientType: clientType,
              appVersion: version));
      final reulstDecode = Auth.fromJson(json.decode(result.data));
      appData.principal = parseJwt(reulstDecode.token.toString());
      await _storage.write(key: 'token', value: reulstDecode.token.toString());
      await _storage.write(
          key: 'refreshToken', value: reulstDecode.refreshToken.toString());

      appData.token = reulstDecode.token;
      return reulstDecode;
    } catch (e) {
      throw e;
    }
  }

  Future doLogout() async {
    try {
      await _storage.delete(key: 'token');
      await _storage.delete(key: 'refreshToken');
      appData.principal = null;
    } catch (e) {
      throw e;
    }
  }

  Future<bool> doRefresh() async {
    try {
      String _token = await _storage.read(key: 'token');
      String _refreshToken = await _storage.read(key: 'refreshToken');
      Response result = await dioRefresh.post('${app.baseAddress}auth/refresh',
          data: Auth(token: _token, refreshToken: _refreshToken));
      final reulstDecode = Auth.fromJson(json.decode(result.data));

      await _storage.delete(key: 'token');
      await _storage.write(key: 'token', value: reulstDecode.token.toString());
      await _storage.delete(key: 'refreshToken');
      await _storage.write(
          key: 'refreshToken', value: reulstDecode.refreshToken.toString());
      appData.token = reulstDecode.token;
      appData.principal = parseJwt(reulstDecode.token.toString());
      return true;
    } catch (e) {
      return false;
    }
  }

  Future checkConnection() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.none) {
      return throw Exception('No Internet Connection');
    }
  }

  Principal parseJwt(String token) {
    final parts = token.split('.');
    if (parts.length != 3) {
      throw Exception('invalid token');
    }

    final payload = _decodeBase64(parts[1]);
    final payloadMap = json.decode(payload);
    if (payloadMap is! Map<String, dynamic>) {
      throw Exception('invalid payload');
    }

    final principal = Principal.fromJson(payloadMap);
    return principal;
  }

  String _decodeBase64(String str) {
    String output = str.replaceAll('-', '+').replaceAll('_', '/');

    switch (output.length % 4) {
      case 0:
        break;
      case 2:
        output += '==';
        break;
      case 3:
        output += '=';
        break;
      default:
        throw Exception('Illegal base64url string!"');
    }

    return utf8.decode(base64Url.decode(output));
  }
}
