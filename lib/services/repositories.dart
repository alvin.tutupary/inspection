import 'package:inspection/model/auth/auth.dart';
import 'package:inspection/model/inspection/inspection.dart';
import 'package:inspection/model/inspection_transaction/inspection_transaction.dart';
import 'package:inspection/services/inspection_service.dart';
import 'package:inspection/services/login_service.dart';

class Repository {
  AuthService auth = AuthService();
  Future<Auth> doLogin(String userName, String password, String clientType,
      String version) async {
    try {
      return await auth.doLogin(userName, password, clientType, version);
    } catch (e) {
      throw e;
    }
  }

  Future<bool> doRefresh() => auth.doRefresh();

  InspectionService inspection = InspectionService();
  Future<List<InspectionTransaction>> getInspectionTransactions(
          bool ispic,
          bool isadmin,
          bool issued,
          String status,
          String search,
          String orderby,
          int page,
          int max) =>
      inspection.getInspectionTransactions(
          ispic, isadmin, issued, status, search, orderby, page, max);

  Future<InspectionTransaction> getInspectionTransactionByPk(String pk) =>
      inspection.getInspectionTransactionByPk(pk);

  Future putInspectionTransactionSubmit(
      UpdateInspectionTransactionSubmit model) async {
    try {
      await inspection.putInspectionTransactionSubmit(model);
    } catch (e) {
      throw e;
    }
  }
}
