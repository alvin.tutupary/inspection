import 'package:flutter/material.dart';
import 'package:inspection/helper/styles.dart';
import 'package:inspection/provider/auth_provider.dart';
import 'package:inspection/provider/inspection_provider.dart';
import 'package:inspection/provider/inspections_provider.dart';
import 'package:provider/provider.dart';

import 'helper/constant.dart';
import 'helper/routers.dart' as Route;

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<AuthProvider>(create: (_) => AuthProvider()),
        ChangeNotifierProvider<InspectionsProvider>(
            create: (_) => InspectionsProvider()),
        ChangeNotifierProvider<InspectionProvider>(
            create: (_) => InspectionProvider()),
      ],
      child: MaterialApp(
        title: 'Facility Inspection',
        initialRoute: Paths.landing,
        onGenerateRoute: Route.Router.generateRoute,
        theme: ThemeData(
          brightness: Brightness.light,
          primaryColor: Style.color3,
        ),
      ),
    );
  }
}
