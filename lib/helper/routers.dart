import 'package:flutter/material.dart';
import 'package:inspection/helper/constant.dart';
import 'package:inspection/ui/eform_detail_page.dart';
import 'package:inspection/ui/eform_page.dart';
import 'package:inspection/ui/inspection_page.dart';
import 'package:inspection/ui/landing_page.dart';
import 'package:inspection/ui/main_page.dart';
import 'package:inspection/ui/login_page.dart';
import 'package:inspection/ui/signature_page.dart';

class Router {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case Paths.main:
        return MaterialPageRoute(builder: (_) => MainPage());
      case Paths.landing:
        return MaterialPageRoute(builder: (_) => LandingPage());
      case Paths.login:
        return MaterialPageRoute(builder: (_) => LoginPage());
      case Paths.inspection:
        return MaterialPageRoute(builder: (_) => InspectionPage());
      case Paths.eform:
        return MaterialPageRoute(builder: (_) => EFormPage());
      case Paths.eformdetail:
        return MaterialPageRoute(builder: (_) => EFormDetailPage());
      case Paths.signature:
        return MaterialPageRoute(builder: (_) => SignaturePage());
      default:
        return MaterialPageRoute(builder: (_) {
          return Scaffold(
            body: Center(
              child: Text('No route defined for ${settings.name}'),
            ),
          );
        });
    }
  }
}
