import 'package:inspection/model/auth/auth.dart';
import 'package:intl/intl.dart';



class MyGlobal {
  static final MyGlobal _instance = MyGlobal._internal();

  factory MyGlobal() => _instance;

  String token;
  bool isBussy;
  String selectedPk;
  bool isLogout;
  Principal principal;
  String formValidation;
  String submitActualStartDate;


  
  String dateFormat(String date){
    if(date != null){
    DateTime dates = DateTime.parse(date);
    DateFormat pattern = DateFormat('dd MMM yy h:mm a'); 
    String dateString = pattern.format(dates);
    return dateString;
    }
    
  }


  MyGlobal._internal() {
    isBussy = false;
  }
}

final appData = MyGlobal();
