import 'package:dio/dio.dart';
import 'package:inspection/helper/appSettings.dart';
import 'package:inspection/helper/global.dart';
import '../services/login_service.dart';

class ClientApi {
  Dio dio;
  Dio dioRefresh;

  ClientApi() {
    BaseOptions option = BaseOptions(
      baseUrl: app.baseAddress,
      responseType: ResponseType.plain,
    );
    dio = new Dio(option);
    RequestOptions _request;
    dio.interceptors.add(InterceptorsWrapper(
      onRequest: (request) {
        if (!request.uri.toString().contains('auth/login') &&
            !request.uri.toString().contains('auth/refresh'))
          request.headers['Authorization'] = 'Bearer ' + appData.token;
        _request = request;
        return _request;
      },
      onResponse: (Response response) {
        if (response.statusCode == 200)
          return response;
        else
          return DioError(error: DioErrorType.RESPONSE);
      },
      onError: (DioError e) async {
        if (e.response?.statusCode == 401 &&
            !_request.uri.toString().contains('auth/login')) {
          dio.interceptors.requestLock.lock();
          dio.interceptors.responseLock.lock();
          bool isRefresh = await AuthService().doRefresh().whenComplete(() {
            dio.interceptors.requestLock.unlock();
            dio.interceptors.responseLock.unlock();
          });
          if (isRefresh) {
            return dio.request(_request.path);
          }
          // return DioError(type: DioErrorType.RESPONSE, error: "Unauthorized");
        }
        return e;
      },
    ));
  }
}
