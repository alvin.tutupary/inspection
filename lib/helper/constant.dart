class Paths {
  static const String landing = 'landing';
  static const String main = 'main';
  static const String login = 'login';
  static const String category = 'category';
  static const String inspection = 'inspection';
  static const String eform = 'eform_page';
  static const String eformdetail = 'eform_detail_page';
  static const String response = 'response_page';
  static const String camera = 'camera_page';
  static const String signature = 'signature_page';
}
