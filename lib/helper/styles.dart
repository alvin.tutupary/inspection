import 'package:flutter/material.dart';

class Style {
  static final color1 = const Color(0xff005073);
  static final color2 = const Color(0xff107dac);
  static final color3 = const Color(0xff2a166d); //(0xff416CF9);
  static final color4 = const Color(0xff1ebbd7);
  static final color5 = const Color(0xff71c7ec);
  static final color6 = const Color(0xff49435e);
  static final color7 = const Color(0xFFF3F4F9);

  static final gColorHorizontal = const LinearGradient(
    colors: [Color(0xff107dac), Color(0xff1ebbd7)],
  );

  static final gColorVertical = const LinearGradient(
    colors: [Color(0xff107dac), Color(0xff1ebbd7)],
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
  );

  static final baseTextStyle = const TextStyle(fontFamily: 'Poppins');
  static final smallTextStyle = commonTextStyle.copyWith(
    fontSize: 9.0,
  );
  static final commonTextStyle = baseTextStyle.copyWith(
      color: const Color(0xffb6b2df),
      fontSize: 14.0,
      fontWeight: FontWeight.w400);
  static final titleTextStyle = baseTextStyle.copyWith(
      color: Colors.white, fontSize: 18.0, fontWeight: FontWeight.w600);
  static final headerTextStyle = baseTextStyle.copyWith(
      color: Colors.white, fontSize: 20.0, fontWeight: FontWeight.w400);

  static Color colorParam(String color) {
    if(color == 'green') return Colors.green;
    else if(color == 'yellow') return Colors.yellow;
    else if(color == 'blue') return Colors.blue;
    else if(color == 'red') return Colors.red;
    else if(color == 'orange') return Colors.orange;
    else return null;
  }
}
