import 'package:flutter/material.dart';

class CompanyPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(15),
      child: ListTile(
        leading: Container(
          height: 70,
          width: 70,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/img/chemistry.png"),
              fit: BoxFit.fitWidth,
            ),
            borderRadius: BorderRadius.circular(12),
          ),
        ),
        title: Text('Item Count: 1'),
        trailing: RaisedButton(
          child: Text('Clear'),
          color: Theme.of(context).buttonColor,
          elevation: 1.0,
          splashColor: Colors.blueGrey,
          onPressed: null,
        ),
      ),
    );
  }
}
