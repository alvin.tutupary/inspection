import 'dart:convert';
import 'dart:io';

import 'package:camera/camera.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:inspection/helper/constant.dart';
import 'package:inspection/helper/styles.dart';
import 'package:inspection/model/inspection_form_template_detail/inspection_form_template_detail.dart';
import 'package:inspection/model/inspection_form_template_header/inspection_form_template_header.dart';
import 'package:inspection/model/inspection_response_detail/inspection_response_detail.dart';
import 'package:inspection/provider/inspection_provider.dart';
import 'package:inspection/shared/diallogs.dart';
import 'package:inspection/shared/loading_modal.dart';
import 'package:photo_view/photo_view.dart';
import 'package:provider/provider.dart';
import 'camera_page.dart';

class EFormDetailPage extends StatefulWidget {
  EFormDetailPage({Key key}) : super(key: key);

  @override
  _EFormDetailPageState createState() => _EFormDetailPageState();
}

class _EFormDetailPageState extends State<EFormDetailPage> {
  MenuButton menuButton;

  @override
  void initState() {
    super.initState();
  }

  Widget widgetLoading(BuildContext context) {
    return Stack(
      children: <Widget>[widgetMain(context), LoadingModal()],
    );
  }

  Widget widgetMain(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.all(15.0),
        child: Consumer<InspectionProvider>(builder: (_, inspectionData, __) {
          if (inspectionData.inspectionFormTemplateHeaders != null &&
              inspectionData.inspectionFormTemplateHeaders.length > 0) {
            return ListView.builder(
              itemCount: inspectionData.inspectionFormTemplateHeaders.length,
              itemBuilder: (ctx, i) {
                final header = inspectionData.inspectionFormTemplateHeaders[i];
                return ListView(
                  shrinkWrap: true,
                  physics: ClampingScrollPhysics(),
                  children: <Widget>[
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10),
                            topRight: Radius.circular(10)),
                        color: Style.color3,
                      ),
                      child: Padding(
                        padding: EdgeInsets.only(left: 10, top: 10, bottom: 10),
                        child: Text(
                          header.header ?? '-',
                          style: Style.titleTextStyle.copyWith(fontSize: 15),
                        ),
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(10),
                              bottomRight: Radius.circular(10)),
                          boxShadow: [
                            BoxShadow(
                              color: Style.color3.withOpacity(0.2),
                              spreadRadius: 3,
                              blurRadius: 4,
                              offset: Offset(3, 4),
                            ),
                          ],
                          color: Colors.white),
                      child: Questions(
                        header: inspectionData.inspectionFormTemplateHeaders[i],
                        headIndex: i,
                      ),
                    ),
                    Padding(padding: EdgeInsets.only(bottom: 10))
                  ],
                );
              },
            );
          } else {
            return CircularProgressIndicator();
          }
        }));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Style.color7,
      appBar: AppBar(
        title: Text('Inspection'),
      ),
      body: Consumer<InspectionProvider>(builder: (_, inspectionData, __) {
        if (inspectionData.inspection != null) {
          return inspectionData.isLoading
              ? widgetLoading(context)
              : widgetMain(context);
        } else {
          return Container(
            child: Center(
              child: CircularProgressIndicator(),
            ),
          );
        }
      }),
      floatingActionButton: MenuButton(),
    );
  }
}

class MenuButton extends StatefulWidget {
  @override
  _MenuButtonState createState() => _MenuButtonState();
}

class _MenuButtonState extends State<MenuButton>
    with SingleTickerProviderStateMixin {
  bool isOpened = false;
  AnimationController _animationController;
  Animation<Color> _btnColor;
  Animation<double> _animationIcon;
  Animation<double> _translateBtn;
  Curve _curve = Curves.easeOut;
  double _fabHeight = 56.0;

  @override
  void initState() {
    _animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 300))
          ..addListener(() {
            setState(() {});
          });
    _animationIcon =
        Tween<double>(begin: 0.0, end: 1.0).animate(_animationController);
    _btnColor = ColorTween(begin: Style.color3, end: Style.color6).animate(
        CurvedAnimation(
            parent: _animationController,
            curve: Interval(0.0, 1.0, curve: Curves.linear)));
    _translateBtn = Tween<double>(begin: _fabHeight, end: -14.0).animate(
      CurvedAnimation(
        parent: _animationController,
        curve: Interval(0.0, 0.75, curve: _curve),
      ),
    );
    super.initState();
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  Widget buttonSubmit(BuildContext context) {
    final inspectionData = Provider.of<InspectionProvider>(context);
    void onSubmit() async {
      String errMsg = '';
      try {
        inspectionData.isLoading = true;
        String message = await inspectionData.putInspectionTransactionSubmit();
        inspectionData.isLoading = false;
        Dialogs.infoDialog(
            context: context,
            body: (message != null && message != '')
                ? "Failed Submit Inspection\n" + message
                : 'Data Submited',
            function: () => (message != null && message != '')
                ? Navigator.of(context).pop()
                : Navigator.popAndPushNamed(context, Paths.main));
      } catch (e) {
        errMsg = e is DioError
            ? e.response?.statusCode == 400
                ? "Bad Request"
                : e.response?.statusCode == 401
                    ? "Unauthorized"
                    : e.error is SocketException
                        ? 'Cannot connect to server'
                        : 'Failed Submit Inspection'
            : e.runtimeType.hashCode == 52733630
                ? "No Internet Connection"
                : "Something Wrong!\n Please contact OM";

        inspectionData.isLoading = false;
        Dialogs.infoDialog(
          context: context,
          body: errMsg,
          function: () => errMsg == "Unauthorized"
              ? Navigator.pushNamed(context, Paths.login)
              : Navigator.of(context).pop(),
        );
      }
    }

    return FloatingActionButton(
      onPressed: () => onSubmit(),
      child: Icon(Icons.send),
      tooltip: 'Submit',
      heroTag: 'BtnSubmit',
      backgroundColor: Style.color3,
    );
  }

  Widget buttonSignature(BuildContext context) {
    return FloatingActionButton(
      onPressed: () => Dialogs.signatureDialog(context),
      child: Icon(Icons.edit),
      tooltip: 'Signature',
      heroTag: 'BtnSignature',
      backgroundColor: Style.color3,
    );
  }

  Widget buttonToggle() {
    return Container(
      child: FloatingActionButton(
        backgroundColor: _btnColor.value,
        onPressed: animated,
        tooltip: 'Toggle',
        heroTag: 'BtnToggle',
        child: AnimatedIcon(
            icon: AnimatedIcons.menu_close, progress: _animationIcon),
      ),
    );
  }

  void animated() {
    if (isOpened) {
      _animationController.forward();
    } else {
      _animationController.reverse();
    }
    isOpened = !isOpened;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        Transform(
          transform:
              Matrix4.translationValues(0.0, _translateBtn.value * 2.0, 0.0),
          child: buttonSubmit(context),
        ),
        Transform(
          transform:
              Matrix4.translationValues(0.0, _translateBtn.value * 1.0, 0.0),
          child: buttonSignature(context),
        ),
        buttonToggle(),
      ],
    );
  }
}

class Questions extends StatefulWidget {
  final InspectionFormTemplateHeader header;
  final int headIndex;
  Questions({Key key, this.header, this.headIndex}) : super(key: key);

  @override
  _QuestionsState createState() => _QuestionsState();
}

class _QuestionsState extends State<Questions> {
  Future<void> navigateCamera(InspectionFormTemplateDetail detail) async {
    WidgetsFlutterBinding.ensureInitialized();
    final cameras = await availableCameras();
    final firstCamera = cameras.first;

    Navigator.push(
        context,
        MaterialPageRoute<void>(
            builder: (BuildContext context) =>
                CameraPage(camera: firstCamera, detail: detail)));
  }

  @override
  Widget build(BuildContext context) {
    final inspectionData = Provider.of<InspectionProvider>(context);
    if (widget.header.inspectionFormTemplateDetail != null &&
        widget.header.inspectionFormTemplateDetail.length > 0) {
      return ListView.builder(
        itemCount: widget.header.inspectionFormTemplateDetail.length,
        shrinkWrap: true,
        physics: ClampingScrollPhysics(),
        itemBuilder: (context, index) {
          final detail = widget.header.inspectionFormTemplateDetail[index];

          return Container(
            padding: EdgeInsets.all(10),
            child: Column(
              children: <Widget>[
                Text(detail.question ?? '-'),
                TextFormField(
                  initialValue: detail.description,
                  onChanged: (value) {
                    inspectionData.setDescription(detail, value);
                  },
                  decoration: InputDecoration(
                    labelText: 'Description',
                    hintText: 'Description',
                    isDense: true,
                  ),
                ),
                TextFormField(
                  initialValue: detail.solution,
                  onChanged: (value) {
                    inspectionData.setSolution(detail, value);
                  },
                  decoration: InputDecoration(
                    labelText: 'Solution',
                    hintText: 'Solution',
                    isDense: true,
                  ),
                ),
                showImage(detail, index, widget.headIndex),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    IconButton(
                      icon: Icon(Icons.camera_enhance),
                      onPressed: () => navigateCamera(detail),
                      color: Style.color3,
                    ),
                    Container(
                      child: detail.model == "Text"
                          ? Flexible(
                              child: TextFormField(
                                initialValue: detail.responseText,
                                onChanged: (textResponse) {
                                  inspectionData.setResponse(
                                      detail, null, textResponse);
                                },
                                decoration: InputDecoration(
                                  labelText: 'Response',
                                  hintText: 'Response',
                                  isDense: true,
                                ),
                              ),
                            )
                          : Response(detail: detail),
                    ),
                  ],
                )
              ],
            ),
          );
        },
      );
    } else {
      return CircularProgressIndicator();
    }
  }

  Widget showImage(
      InspectionFormTemplateDetail detail, int childIndex, int headIndex) {
    if (detail.images != null && detail.images.length > 0) {
      return Container(
        height: 50.0,
        child: ListView.builder(
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            itemCount: detail.images.length,
            itemBuilder: (ctx, index) {
              return GestureDetector(
                onTap: () =>
                    Navigator.push(context, MaterialPageRoute(builder: (_) {
                  return ImagePreview(
                    base64: detail.images[index].base64string,
                    detail: detail,
                    index: index,
                    childIndex: childIndex,
                    headIndex: headIndex,
                  );
                })),
                child: Padding(
                  padding: EdgeInsets.symmetric(vertical: 5),
                  child: Image.memory(
                    base64Decode(detail.images[index].base64string),
                    height: 100,
                    width: 100,
                  ),
                ),
              );
            }),
      );
    } else {
      return Container();
    }
  }
}

class Response extends StatelessWidget {
  final InspectionFormTemplateDetail detail;
  const Response({Key key, this.detail});

  @override
  Widget build(BuildContext context) {
    final inspectionData = Provider.of<InspectionProvider>(context);
    if (detail.pkInspectionResponseNavigation == null) {
      return CircularProgressIndicator();
    } else {
      return Container(
        child: DropdownButton<InspectionResponseDetail>(
          hint: const Text("Not selected"),
          value: detail.response,
          onChanged: (final InspectionResponseDetail _value) {
            inspectionData.setResponse(detail, _value, null);
          },
          items: detail.pkInspectionResponseNavigation.inspectionResponseDetail
              .map<DropdownMenuItem<InspectionResponseDetail>>(
            (final InspectionResponseDetail _value) {
              return DropdownMenuItem<InspectionResponseDetail>(
                value: _value ?? null,
                child: Container(
                  width: 100,
                  color: Style.colorParam(_value.color),
                  child: Text(_value.choiceText),
                ),
              );
            },
          ).toList(),
        ),
      );
    }
  }
}

class ImagePreview extends StatefulWidget {
  final String base64;
  final InspectionFormTemplateDetail detail;
  final int index;
  final int childIndex;
  final int headIndex;
  ImagePreview(
      {Key key,
      this.base64,
      this.detail,
      this.index,
      this.childIndex,
      this.headIndex})
      : super(key: key);

  @override
  ImagePreviewState createState() => ImagePreviewState();
}

class ImagePreviewState extends State<ImagePreview> {
  @override
  Widget build(BuildContext context) {
    final inspectionData = Provider.of<InspectionProvider>(context);
    void onDelete(
      InspectionFormTemplateDetail detail,
      int index,
      int childIndex,
      int headIndex,
    ) {
      inspectionData
          .inspection
          .pkInspectionFormTemplateNavigation
          .inspectionFormTemplateHeader[headIndex]
          .inspectionFormTemplateDetail[childIndex]
          .images
          .removeAt(index);

      Navigator.pop(context);
    }

    return Scaffold(
      backgroundColor: Colors.black,
      bottomNavigationBar: Container(
        color: Colors.transparent,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            RaisedButton(
              onPressed: () {
                onDelete(widget.detail, widget.index, widget.childIndex,
                    widget.headIndex);
              },
              child: Text('Delete'),
            ),
            RaisedButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text('Cancel'),
            )
          ],
        ),
      ),
      body: Container(
        child: PhotoView(
          imageProvider: MemoryImage(base64Decode(widget.base64)),
        ),
      ),
    );
  }
}
