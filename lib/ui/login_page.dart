import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:inspection/helper/constant.dart';
import 'package:inspection/helper/styles.dart';
import 'package:inspection/services/repositories.dart';
import 'package:inspection/shared/diallogs.dart';
import 'package:package_info/package_info.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key key}) : super(key: key);
  @override
  State<StatefulWidget> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final formKey = new GlobalKey<FormState>();
  String username;
  String password;

  bool isLoading;

  @override
  void initState() {
    isLoading = false;
    username = '';
    password = '';
    SystemChrome.setEnabledSystemUIOverlays([]);
    super.initState();
  }

  Future<bool> onBack() {
    return showDialog(
        context: context,
        builder: (ctx) {
          return AlertDialog(
            title: Text('Close App'),
            content: Text('Wanna Close App?'),
            actions: <Widget>[
              FlatButton(
                child: Text('YES'),
                onPressed: () => SystemNavigator.pop(),
              ),
              FlatButton(
                child: Text('NO'),
                onPressed: () => Navigator.of(context).pop(),
              )
            ],
          );
        });
  }

  void doLogin() async {
    try {
      setState(() {
        isLoading = true;
      });
      final key = formKey.currentState;

      // checkForm();
      if (key.validate()) {
        key.save();
        PackageInfo packageInfo = await PackageInfo.fromPlatform();
        await Repository()
            .doLogin(username, password, 'Mobile', packageInfo.version);
        Navigator.popAndPushNamed(context, Paths.main);
        setState(() {
          username = '';
          password = '';
        });
      }
      setState(() {
        isLoading = false;
      });
    } catch (e) {
      var errMsg = '';
      errMsg = e is DioError
          ? e.response?.statusCode == 400
              ? "Bad Request"
              : e.response?.statusCode == 401
                  ? "Wrong username or password"
                  : e.error is SocketException
                      ? 'Cannot connect to server'
                      : 'Login Failed'
          : e.runtimeType.hashCode == 52733630
              ? "No Internet Connection"
              : "Something Wrong!\n Please contact OM";
      Dialogs.infoDialog(
          context: context,
          body: errMsg,
          function: () {
            setState(() {
              isLoading = false;
            });
            Navigator.of(context).pop();
          });
      setState(() {
        isLoading = false;
      });
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  Widget widgetMain(BuildContext context) {
    return Center(
      child: ListView(
        shrinkWrap: true,
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.only(top: 30),
            child: Card(
              color: Colors.white,
              elevation: 6.0,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
              ),
              margin: EdgeInsets.only(right: 15.0, left: 15.0),
              child: Container(
                padding: EdgeInsets.all(20),
                child: Form(
                  key: formKey,
                  child: Column(
                    children: <Widget>[
                      Align(
                          alignment: Alignment.center,
                          child: Image.asset(
                            'assets/img/logo.png',
                            scale: 2,
                          ),
                          widthFactor: 20),
                      Padding(padding: EdgeInsets.only(bottom: 15)),
                      TextFormField(
                        validator: (value) =>
                            value.isEmpty ? 'Please insert username' : null,
                        onSaved: (value) => username = value,
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.symmetric(vertical: 1),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          prefixIcon: Icon(
                            Icons.person,
                          ),
                          hintText: 'Username',
                        ),
                      ),
                      Padding(padding: EdgeInsets.only(bottom: 30)),
                      TextFormField(
                        obscureText: true,
                        validator: (value) =>
                            value.isEmpty ? 'Please insert password' : null,
                        onSaved: (value) => password = value,
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.symmetric(vertical: 1),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          prefixIcon: Icon(
                            Icons.vpn_key,
                          ),
                          hintText: 'Password'
                        ),
                      ),
                      Padding(padding: EdgeInsets.symmetric(vertical: 15)),
                      Container(
                        height: 45,
                        width: MediaQuery.of(context).size.width / 1.2,
                        decoration: BoxDecoration(
                            color: Style.color3,
                            borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                        child: RaisedButton(
                          onPressed: () {
                            doLogin();
                          },
                          child: Padding(
                              padding: const EdgeInsets.symmetric(vertical: 8),
                              child: isLoading
                                  ? Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Container(
                                          width: 10,
                                          height: 10,
                                          child: CircularProgressIndicator(
                                            valueColor:
                                                AlwaysStoppedAnimation<Color>(
                                                    Colors.white),
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 10),
                                        ),
                                        Text(
                                          'Loading',
                                          style: Style.titleTextStyle
                                              .copyWith(fontSize: 15),
                                        )
                                      ],
                                    )
                                  : Text(
                                      'Login',
                                      style: Style.titleTextStyle
                                          .copyWith(fontSize: 15),
                                    )),
                          color: Theme.of(context).primaryColor,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10)),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Style.color3,
        body: WillPopScope(
            onWillPop: onBack,
            child: widgetMain(context)
            ),
      ),
    );
  }
}
