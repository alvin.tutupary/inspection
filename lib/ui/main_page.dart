import 'package:flutter/material.dart';
import 'package:inspection/helper/constant.dart';
import 'package:inspection/services/login_service.dart';
import 'package:inspection/ui/account_page.dart';
import '../helper/styles.dart';
import 'home_page.dart';

class MainPage extends StatefulWidget {
  MainPage({Key key}) : super(key: key);

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  int _selectedIndex = 0;
  // static TextStyle optionStyle =
  //     TextStyle(fontSize: 30, fontWeight: FontWeight.bold);

  List<Widget> _widgetOptions = <Widget>[
    HomePage(),
    AccountPage(),
  ];
 
  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  Future<bool> _doLogout() {
    return showDialog(
        context: context,
        builder: (ctx) {
          return AlertDialog(
            title: new Text('Logout'),
            content: new Text('are you sure want to logout?'),
            actions: <Widget>[
              FlatButton(
                child: Text('YES'),
                onPressed: () => {
                  AuthService().doLogout(),
                  Navigator.pushNamed(context, Paths.login)
                }
              ),
              FlatButton(
                child: Text('NO'),
                onPressed: () => Navigator.of(context).pop(),
              )
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
      
    return WillPopScope(
      onWillPop: _doLogout,
      child: Scaffold(
        backgroundColor: Style.color7,
        body: Container(
          child: Center(
            child: _widgetOptions.elementAt(_selectedIndex),
          ),
        ),
        bottomNavigationBar: SafeArea(
          child: BottomNavigationBar(
            fixedColor: Style.color3,
            items: const <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                icon: Icon(Icons.home),
                title: Text('Home'),
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.person),
                title: Text('Account'),
              ),
            ],
            currentIndex: _selectedIndex,
            onTap: _onItemTapped,
          ),
        ),
      ),
    );
  }
}
