import 'dart:developer';
import 'dart:ui' as ui;
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_signature_pad/flutter_signature_pad.dart';
import 'package:inspection/helper/styles.dart';
import 'package:inspection/provider/inspection_provider.dart';
import 'package:provider/provider.dart';

class SignaturePage extends StatefulWidget {
  SignaturePage({Key key}) : super(key: key);

  @override
  _SignaturePageState createState() => _SignaturePageState();
}

class _SignaturePageState extends State<SignaturePage> {
  final _sign = GlobalKey<SignatureState>();
  ui.Image image;
  bool hasImage;
  final inspectorName = TextEditingController();

  @override
  void initState() {
    super.initState();
    hasImage = false;
    inspectorName.text = '';
  }

  @override
  Widget build(BuildContext context) {
    final inspectionData = Provider.of<InspectionProvider>(context);
    if (inspectionData.inspection.signature != null &&
        inspectionData.inspection.signature.isNotEmpty) {
      setState(() {
        hasImage = true;
      });
    }

    void onSave() async {
      final sign = _sign.currentState;
      final image = await sign.getData();
      var data = await image.toByteData(format: ui.ImageByteFormat.png);

      inspectionData.inspection.signature =
          base64.encode(data.buffer.asUint8List());
      inspectionData.inspection.inspectorName = inspectorName.text;
      setState(() {
        hasImage = true;
      });
      Navigator.of(context).pop();
    }

    void onClear() {
      try {
        final sign = _sign.currentState;
        inspectionData.inspection.signature = null;
        inspectionData.inspection.inspectorName = null;
        if (sign != null) sign.clear();
        setState(() {
          hasImage = false;
        });
      } catch (e) {
        log(e.toString());
      }
    }

    Widget showImage() {
      final decodedBytes = base64Decode(inspectionData.inspection.signature);
      return Container(
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 5),
          child: Image.memory(
            decodedBytes,
            height: 150,
          ),
        ),
      );
    }

    return Consumer<InspectionProvider>(builder: (_, inspectionData, __) {
      return AlertDialog(
        content: !hasImage 
        ?Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(
              height: 150,
              child: Signature(
                color: Colors.black,
                key: _sign,
                strokeWidth: 2.0,
              ),
              decoration: BoxDecoration(
                  border: Border.all(color: Style.color3, width: 1.5)),
            ),
            TextField(
              controller: inspectorName,
              decoration: InputDecoration(
                hintText: 'Insert your name',
              ),
            )
          ],
        )
        : Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            showImage(),
            Text(inspectionData.inspection.inspectorName??'-'),
          ],
        ),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        actions: <Widget>[
          FlatButton(
            onPressed: () => onSave(),
            child: const Text('Save'),
          ),
          FlatButton(
            onPressed: () => onClear(),
            child: const Text('Clear'),
          ),
          FlatButton(
            onPressed: () => Navigator.of(context).pop(),
            child: const Text('Cancel'),
          )
        ],
      );
    });
  }
}
