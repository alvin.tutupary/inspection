import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:inspection/helper/constant.dart';
import 'package:inspection/helper/global.dart';
import 'package:inspection/model/inspection_transaction/inspection_transaction.dart';
import 'package:inspection/provider/inspections_provider.dart';
import 'package:inspection/shared/diallogs.dart';
import 'package:inspection/shared/loading_modal.dart';
import 'package:provider/provider.dart';

import '../helper/styles.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  bool isLoading;

  @override
  void initState() {
    isLoading = false;
    super.initState();
  }

  void navigate(BuildContext context) async {
    String errMsg;
    final inspectionData =
        Provider.of<InspectionsProvider>(context, listen: false);
    try {
      setState(() {
        isLoading = true;
      });
      inspectionData.setPage = 1;
      inspectionData.setMax = 5;

      inspectionData.inspections = List<InspectionTransaction>();
      inspectionData.inspections = await inspectionData.getInspectionTransactions();
      inspectionData.setMaxPage = inspectionData.inspections.length > 0 ? (inspectionData.inspections[0].records / inspectionData.max).round() : 5;
          setState(() {
            isLoading = false;
          });
      Navigator.pushNamed(context, Paths.inspection);
    } catch (e) {
      errMsg = e is DioError
          ? e.response?.statusCode == 400
              ? "Bad Request"
              : e.response?.statusCode == 401
                  ? "Unauthorized"
                  : e.error is SocketException
                      ? 'Cannot connect to server'
                      : 'Login Failed'
          : e.runtimeType.hashCode == 52733630
              ? "No Internet Connection"
              : "Something Wrong!\n Please contact OM";
      setState(() {
        isLoading = false;
      });
      Dialogs.infoDialog(
        context: context,
        body: errMsg,
        function: () => errMsg == "Unauthorized"
            ? Navigator.pushNamed(context, Paths.login)
            : Navigator.of(context).pop(),
      );
    }
  }

  Widget widgetLoading(BuildContext context) {
    return Stack(
      children: <Widget>[widgetMain(context), LoadingModal()],
    );
  }

  Widget widgetMain(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(15),
      child: ListView(
        children: <Widget>[
          Container(
              margin: EdgeInsets.symmetric(vertical: 30),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(18),
                  boxShadow: [
                    BoxShadow(
                      color: Style.color3.withOpacity(0.3),
                      spreadRadius: 3,
                      blurRadius: 4,
                      offset: Offset(3, 4),
                    ),
                  ],
                  color: Theme.of(context).primaryColor),
              height: 150,
              child: Row(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 15),
                    child: CircleAvatar(
                      child: Text(
                        appData.principal.fullName
                            .substring(0, 1)
                            .toUpperCase(),
                        style: TextStyle(color: Style.color3, fontSize: 80),
                      ),
                      backgroundColor: Colors.white,
                      radius: 45,
                    ),
                  ),
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      Text('Hi, Welcome', style: Style.headerTextStyle),
                      Text(
                        appData.principal.fullName,
                        style: Style.titleTextStyle,
                      ),
                    ],
                  )
                ],
              )),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              GestureDetector(
                onTap: () => navigate(context),
                child: Container(
                    height: 100,
                    width: 100,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(18),
                        boxShadow: [
                          BoxShadow(
                            color: Style.color3.withOpacity(0.2),
                            spreadRadius: 3,
                            blurRadius: 4,
                            offset: Offset(3, 4),
                          ),
                        ],
                        color: Colors.white),
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.symmetric(vertical: 5),
                          child: Image.asset(
                            'assets/img/test.png',
                            scale: 8,
                          ),
                        ),
                        Text(
                          'Inspector',
                          style: Style.commonTextStyle
                              .copyWith(color: Style.color3),
                        )
                      ],
                    )),
              ),
            ],
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return isLoading ? widgetLoading(context) : widgetMain(context);
  }
}
