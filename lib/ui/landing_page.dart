import 'dart:async';
import 'dart:convert';
import 'dart:developer';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:inspection/helper/constant.dart';
import 'package:inspection/helper/global.dart';
import 'package:inspection/helper/styles.dart';
import 'package:inspection/model/auth/auth.dart';
import 'package:inspection/helper/appSettings.dart';

class LandingPage extends StatefulWidget {
  LandingPage({Key key}) : super(key: key);

  @override
  _LandingPageState createState() => _LandingPageState();
}

class _LandingPageState extends State<LandingPage> {
  bool isLoading = true;
  String errMsg;
  final _storage = FlutterSecureStorage();

  @override
  void initState() {
    errMsg = '';
    super.initState();
    Timer(Duration(seconds: 3), () => onStart());
  }

  onStart() async {
    try {
      var connectivityResult = await (Connectivity().checkConnectivity());
      bool isAuthenticated = await checkIfAuthenticated();
      if (connectivityResult == ConnectivityResult.none) {
        setState(() {
          errMsg = 'No internet connection';
        });
      } else {
        isAuthenticated
            ? Navigator.pushReplacementNamed(context, Paths.main)
            : Navigator.pushReplacementNamed(context, Paths.login);
      }
    } catch (e) {
      setState(() {
        errMsg = 'Something went wrong! please contact OM';
      });
    }
  }

  Future<bool> checkIfAuthenticated() async {
    try {
      bool result = false;
      var jwt = await _storage.read(key: "token");
      var refresh = await _storage.read(key: "refreshToken");

      if (jwt != null && refresh != null) {
        final parts = jwt.split('.');
        if (parts.length != 3) {
          return false;
        }
        var payload = json
            .decode(ascii.decode(base64.decode(base64.normalize(parts[1]))));
        if (DateTime.fromMillisecondsSinceEpoch(payload["exp"] * 1000)
            .isAfter(DateTime.now())) {
          appData.principal = parseJwt(jwt.toString());
          appData.token = jwt;
          result = true;
        } else {
          result = false;
        }
      }

      return result;
    } catch (e) {
      log(e.toString());
      return false;
    }
  }

  Principal parseJwt(String token) {
    final parts = token.split('.');
    if (parts.length != 3) {
      throw Exception('invalid token');
    }

    final payload = _decodeBase64(parts[1]);
    final payloadMap = json.decode(payload);
    if (payloadMap is! Map<String, dynamic>) {
      throw Exception('invalid payload');
    }

    final principal = Principal.fromJson(payloadMap);
    return principal;
  }

  String _decodeBase64(String str) {
    String output = str.replaceAll('-', '+').replaceAll('_', '/');

    switch (output.length % 4) {
      case 0:
        break;
      case 2:
        output += '==';
        break;
      case 3:
        output += '=';
        break;
      default:
        throw Exception('Illegal base64url string!"');
    }

    return utf8.decode(base64Url.decode(output));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Style.color3,
      body: Container(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image.asset(
                'assets/img/logo.png',
                scale: 2,
              ),
              
              Padding(
                padding: EdgeInsets.only(top: 20),
              ),
              Text("Desk Inspection ${app.version}", style: Style.headerTextStyle,),
Padding(padding: EdgeInsets.symmetric(vertical: 30)),
              (errMsg == null || errMsg.isEmpty)
                  ? CircularProgressIndicator(
                      valueColor:
                          new AlwaysStoppedAnimation<Color>(Colors.white),
                    )
                  : Text(
                      errMsg,
                      style: Style.commonTextStyle,
                    )
            ],
          ),
        ),
      ),
    );
  }
}
