import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:inspection/helper/constant.dart';
import 'package:inspection/helper/global.dart';
import 'package:inspection/provider/inspection_provider.dart';
import 'package:inspection/provider/inspections_provider.dart';
import 'package:inspection/shared/diallogs.dart';
import 'package:inspection/shared/info_detail.dart';
import 'package:inspection/shared/loading_modal.dart';
import 'package:provider/provider.dart';
import '../helper/styles.dart';

class EFormPage extends StatefulWidget {
  EFormPage({Key key}) : super(key: key);

  @override
  _EFormPageState createState() => _EFormPageState();
}

class _EFormPageState extends State<EFormPage> {
  bool isLoading;

  @override
  void initState() {
    isLoading = false;
    super.initState();
  }

  void navigate(BuildContext context) async {
    final inspection = Provider.of<InspectionProvider>(context, listen: false);

    String errMsg = '';
    try {
      setState(() {
        isLoading = true;
      });

      appData.submitActualStartDate = DateTime.now().toString();
      inspection.inspection = null;
      inspection.inspection =
          await inspection.getInspectionTransactionByPk(appData.selectedPk);

      if (inspection.inspection != null) {
        if (DateTime.now().add(new Duration(hours: 7)).isBefore(
            DateTime.parse(inspection.inspection.inspectionDate))) {
          errMsg = "not yet time for inspection, wait " +
              MyGlobal().dateFormat(inspection.inspection.pkNavigation.planStartDate);
          inspection.inspection = null;
        } else {
          Navigator.pushNamed(context, Paths.eformdetail);
        }
      } else {
        errMsg = "inspection not found, please contact OM";
      }
      setState(() {
        isLoading = false;
      });
      if (errMsg != '') {
        Dialogs.infoDialog(
          context: context,
          body: errMsg,
          function: () => Navigator.of(context).pop(),
        );
      }
    } catch (e) {
      errMsg = e is DioError
          ? e.response?.statusCode == 400
              ? "Bad Request"
              : e.response?.statusCode == 401
                  ? "Unauthorized"
                  : e.error is SocketException
                      ? 'Cannot connect to server'
                      : 'Login Failed'
          : e.runtimeType.hashCode == 52733630
              ? "No Internet Connection"
              : "Something Wrong!\n Please contact OM";
      setState(() {
        isLoading = false;
      });
      Dialogs.infoDialog(
        context: context,
        body: errMsg,
        function: () => errMsg == "Unauthorized"
            ? Navigator.pushNamed(context, Paths.login)
            : Navigator.of(context).pop(),
      );
    }
  }

  Widget widgetLoading(BuildContext context) {
    return Stack(
      children: <Widget>[widgetMain(context), LoadingModal()],
    );
  }

  Widget widgetMain(BuildContext context) {
    final inspectionData =
        Provider.of<InspectionsProvider>(context, listen: false);
    final inspections = inspectionData.findById(appData.selectedPk);

    return Container(
      padding: EdgeInsets.symmetric(horizontal: 15),
      child: Consumer<InspectionsProvider>(
          builder: (_, inspectionsData, __) => inspectionsData.inspections !=
                      null &&
                  inspectionsData.inspections.length > 0
              ? ListView(
                  children: <Widget>[
                    Padding(padding: EdgeInsets.symmetric(vertical: 15)),
                    Container(
                      padding: EdgeInsets.symmetric(vertical: 15),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10),
                            topRight: Radius.circular(10),
                          ),
                          boxShadow: [
                            BoxShadow(
                              color: Style.color3.withOpacity(0.2),
                              spreadRadius: 3,
                              blurRadius: 4,
                              offset: Offset(3, 4),
                            ),
                          ],
                          color: Theme.of(context).primaryColor),
                      child: Center(
                        child: new Text(
                            inspections.pkInspectionFormTemplateNavigation.name
                                    .toUpperCase() ??
                                '-',
                            style: Style.titleTextStyle
                                .copyWith(color: Colors.white, fontSize: 15)),
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(10),
                            bottomRight: Radius.circular(10),
                          ),
                          boxShadow: [
                            BoxShadow(
                              color: Style.color3.withOpacity(0.2),
                              spreadRadius: 3,
                              blurRadius: 4,
                              offset: Offset(3, 4),
                            ),
                          ],
                          color: Colors.white),
                      height: 300,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(padding: EdgeInsets.only(top: 15)),
                          InfoDetail(
                            icon: Icons.add_circle_outline,
                            title: inspections.pkNavigation.code ?? '-',
                          ),
                          Divider(),
                          InfoDetail(
                            icon: Icons.location_on,
                            title: inspections.pkNavigation.location ?? '-',
                          ),
                          Divider(),
                          InfoDetail(
                            icon: Icons.date_range,
                            title:
                                MyGlobal().dateFormat(inspections.inspectionDate) ?? '-',
                          ),
                          Divider(),
                          InfoDetail(
                            icon: Icons.person,
                            title: inspections
                                    .pkInspectionPicNavigation.fullname ??
                                '-',
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 15),
                    ),
                    RaisedButton(
                      onPressed: () => navigate(context),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(vertical: 8),
                        child: Text(
                          'Next',
                          style: Style.titleTextStyle.copyWith(fontSize: 15),
                        ),
                      ),
                      color: Theme.of(context).primaryColor,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),
                    )
                  ],
                )
              : Container(
                  child: Center(
                    child: CircularProgressIndicator(),
                  ),
                )),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Style.color7,
        appBar: AppBar(
          title: Text('Inspection Detail'),
        ),
        body: isLoading ? widgetLoading(context) : widgetMain(context));
  }
}
