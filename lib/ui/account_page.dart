import 'package:flutter/material.dart';
import 'package:inspection/helper/constant.dart';
import 'package:inspection/helper/global.dart';
import 'package:inspection/services/login_service.dart';
import 'package:inspection/shared/info_detail.dart';
import '../helper/styles.dart';

class AccountPage extends StatefulWidget {
  AccountPage({Key key}) : super(key: key);

  @override
  _AccountPageState createState() => _AccountPageState();
}

class _AccountPageState extends State<AccountPage> {
  Future<bool> _doLogout() {
    return showDialog(
        context: context,
        builder: (ctx) {
          return AlertDialog(
            title: new Text('Logout'),
            content: new Text('are you sure want to logout?'),
            actions: <Widget>[
              FlatButton(
                  child: Text('YES'),
                  onPressed: () => {
                        AuthService().doLogout(),
                        Navigator.pushNamed(context, Paths.login)
                      }),
              FlatButton(
                child: Text('NO'),
                onPressed: () => Navigator.of(context).pop(),
              )
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(15),
      child: appData.principal == null
          ? Text('Session out back to login')
          : ListView(
              children: <Widget>[
                Container(
                    margin: EdgeInsets.symmetric(vertical: 30),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(18),
                        boxShadow: [
                          BoxShadow(
                            color: Style.color3.withOpacity(0.3),
                            spreadRadius: 3,
                            blurRadius: 4,
                            offset: Offset(3, 4),
                          ),
                        ],
                        color: Style.color3),
                    height: 150,
                    child: Row(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 15),
                          child: CircleAvatar(
                            child: Text(
                              appData.principal.fullName
                                  .substring(0, 1)
                                  .toUpperCase(),
                              style:
                                  TextStyle(color: Style.color3, fontSize: 80),
                            ),
                            backgroundColor: Colors.white,
                            radius: 45,
                          ),
                        ),
                        Column(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: <Widget>[
                            Text(
                              appData.principal.fullName,
                              style: Style.titleTextStyle,
                            ),
                            Text(appData.principal.email,
                                style: Style.baseTextStyle
                                    .copyWith(color: Colors.white)),
                            Text(
                              appData.principal.nik,
                              style: Style.titleTextStyle,
                            ),
                          ],
                        )
                      ],
                    )),
                Container(
                  margin: EdgeInsets.symmetric(vertical: 5),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: [
                        BoxShadow(
                          color: Style.color3.withOpacity(0.2),
                          spreadRadius: 3,
                          blurRadius: 4,
                          offset: Offset(3, 4),
                        ),
                      ],
                      color: Colors.white),
                  height: 400,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(padding: EdgeInsets.only(top: 15)),
                      InfoDetail(
                        title:
                            'Department : ${appData.principal.departmentName ?? '-'}',
                      ),
                      Divider(),
                      InfoDetail(
                        title: 'Divisi: ${appData.principal.divisiName ?? '-'}',
                      ),
                      Divider(),
                      InfoDetail(
                        title:
                            'Instance : ${appData.principal.nameInstance ?? '-'}',
                      ),
                      Divider(),
                      InfoDetail(
                        title: 'Site: ${appData.principal.site ?? '-'}',
                      ),
                    ],
                  ),
                ),
                RaisedButton(
                  onPressed: () => _doLogout(),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 8),
                    child: Text(
                      'Logout',
                      style: Style.titleTextStyle.copyWith(fontSize: 15),
                    ),
                  ),
                  color: Theme.of(context).primaryColor,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                ),
              ],
            ),
    );
  }
}
