import 'package:flutter/material.dart';
import 'package:inspection/provider/inspections_provider.dart';
import 'package:inspection/widgets/audit_page.dart';
import 'package:provider/provider.dart';
import '../helper/styles.dart';

class InspectionPage extends StatefulWidget {
  InspectionPage({Key key}) : super(key: key);

  @override
  _InspectionPageState createState() => _InspectionPageState();
}

class _InspectionPageState extends State<InspectionPage> {


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Style.color7,
        appBar: AppBar(
          title: Text('Inspection List'),
        ),
        body: Consumer<InspectionsProvider>(
          builder: (_, inspection, __) {
            return inspection.inspections == null
                ? Container(
                    child: Center(
                    child: CircularProgressIndicator(),
                  ))
                : inspection.inspections.length == 0
                    ? Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            height: 50,
                            child: Center(child: Text('Data is empty')),
                          ),
                        ],
                      )
                    : AuditPage();
          },
        ));
  }
}
