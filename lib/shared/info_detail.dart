import 'package:flutter/material.dart';
import 'package:inspection/helper/styles.dart';

class InfoDetail extends StatelessWidget {
  final IconData icon;
  final String title;
  const InfoDetail({Key key, this.icon, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Icon(icon),
      title: Text(
        title ?? '',
        overflow: TextOverflow.ellipsis,
        maxLines: 3,
        style: Style.headerTextStyle.copyWith(color: Colors.grey, fontSize: 15),
      ),
    );
  }
}
