import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:inspection/ui/signature_page.dart';

enum DialogAction { yes, abort }

class Dialogs {
  static Future<DialogAction> yesAbortDialog({
    BuildContext context,
    String body,
    Function function,
  }) async {
    final action = await showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            content: Text(
              body,
            ),
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            actions: <Widget>[
              FlatButton(
                onPressed: () => Navigator.of(context).pop(),
                child: const Text('No'),
              ),
              FlatButton(
                onPressed: () {
                  function();
                },
                child: const Text(
                  'Yes',
                ),
              )
            ],
          );
        });

    return (action != null) ? action : DialogAction.abort;
  }

  static Future<DialogAction> unauthorizedDialog({
    BuildContext context,
    String body,
    Function function,
  }) async {
    final action = await showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            content: Text(
              body,
            ),
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            actions: <Widget>[
              FlatButton(
                onPressed: () {
                  function();
                },
                child: const Text(
                  'OK',
                ),
              )
            ],
          );
        });

    return (action != null) ? action : DialogAction.abort;
  }

  static Future<DialogAction> signatureDialog(BuildContext context) async {
    final action = await showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return SignaturePage();
        });

    return (action != null) ? action : DialogAction.abort;
  }

  static Future<DialogAction> infoDialog({
    BuildContext context,
    String body,
    Function function,
  }) async {
    final action = await showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return Dialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0)),
              child: Container(
                  height: 180.0,
                  width: 150.0,
                  decoration:
                      BoxDecoration(borderRadius: BorderRadius.circular(20.0)),
                  child: SingleChildScrollView(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                          height: 30.0,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(10.0),
                                topRight: Radius.circular(10.0),
                              ),
                              color: Theme.of(context).primaryColor),
                        ),
                        Text(
                          body,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 14.0,
                            fontWeight: FontWeight.w300,
                          ),
                        ),
                        FlatButton(
                            child: Center(
                              child: Text(
                                'OK',
                                style: TextStyle(
                                    fontSize: 14.0,
                                    color: Theme.of(context).primaryColor),
                              ),
                            ),
                            onPressed: () => function(),
                            color: Colors.transparent)
                      ],
                    ),
                  )));
        });

    return (action != null) ? action : DialogAction.abort;
  }
}
