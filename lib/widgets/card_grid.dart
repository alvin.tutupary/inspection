import 'package:flutter/material.dart';

import '../helper/styles.dart';

class CardGrid extends StatelessWidget {
  CardGrid();
  @override
  Widget build(BuildContext context) {
    return Container(
      child: GridView.count(
        crossAxisCount: 3,
        children: List.generate(6, (index) {
          return Container(
            padding: EdgeInsets.all(15),
            child: Container(
              child: Card(
                child: Column(
                  children: <Widget>[
                    Expanded(
                      flex: 3,
                      child: Align(
                          alignment: Alignment.center,
                          child: Image.asset(
                            'assets/img/inspection.png',
                            scale: 10,
                          ),
                          widthFactor: 20),
                    ),
                    Expanded(
                      flex: 1,
                      child: Text(
                        'Item $index',
                        style: Theme.of(context).textTheme.bodyText1,
                      ),
                    ),
                  ],
                ),
              ),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(3.0),
                  boxShadow: [
                    BoxShadow(
                      color: Style.color6,
                      blurRadius: 10.0,
                      offset: Offset(3, 5),
                    ),
                  ]),
            ),
          );
        }),
      ),
    );
  }
}
