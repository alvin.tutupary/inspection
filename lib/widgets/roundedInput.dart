import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class RoundedInput extends StatelessWidget {
  final String validationText;
  final String hintText;
  final IconData icon;
  final Function(String) textFunc;
  final bool isPassword;

  RoundedInput(
      {this.validationText,
      this.hintText,
      this.icon,
      this.textFunc,
      this.isPassword});

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      obscureText: isPassword,
      validator: (value) => value.isEmpty ? validationText : null,
      onSaved: textFunc,
      decoration: InputDecoration(
        border: OutlineInputBorder(
            gapPadding: 0.0, borderRadius: BorderRadius.circular(10.0)),
        prefixIcon: Icon(
          icon,
        ),
        hintText: hintText,
      ),
    );
  }
}
