import 'package:flutter/material.dart';
import 'package:inspection/helper/constant.dart';
import 'package:inspection/helper/global.dart';
import 'package:inspection/helper/styles.dart';
import 'package:inspection/model/inspection_transaction/inspection_transaction.dart';
import 'package:inspection/provider/inspections_provider.dart';
import 'package:inspection/services/repositories.dart';
import 'package:provider/provider.dart';

class AuditPage extends StatefulWidget {
  AuditPage({Key key}) : super(key: key);

  @override
  _AuditPageState createState() => _AuditPageState();
}

class _AuditPageState extends State<AuditPage> {
  bool isLoading = false;
  int page;
  int maxPage;
  int max;
  var inspections = List<InspectionTransaction>();

  @override
  void initState() {
    final inspectionData =
        Provider.of<InspectionsProvider>(context, listen: false);
    inspections = inspectionData.inspections;
    if (inspections != null && inspections.length > 0) {
      int count = (inspections[0].records / 5).ceil();
      inspectionData.setMaxPage = count;
    }
    maxPage = inspectionData.maxPage;
    page = inspectionData.page + 1;
    max = inspectionData.max;
    super.initState();
  }

  Future _loadData() async {
    final inspectionData =
        Provider.of<InspectionsProvider>(context, listen: false);
    var newData = await Repository().getInspectionTransactions(
        true, null, true, 'Issued', '', '', page, max);
      
    setState(() {
      page++;
      inspections.addAll(newData);
      isLoading = false;
    });
    inspectionData.inspections = inspections;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Expanded(
            child: NotificationListener<ScrollNotification>(
                onNotification: (ScrollNotification scrollNotif) {
                  if (!isLoading &&
                      scrollNotif.metrics.pixels ==
                          scrollNotif.metrics.maxScrollExtent &&
                      (page <= maxPage)) {
                    setState(() {
                      isLoading = true;
                    });
                    _loadData();
                  }
                  return isLoading;
                },
                child: ListView.builder(
                  itemCount: inspections.length,
                  itemBuilder: (context, i) => EForms(
                    formName:
                        "${inspections[i].pkInspectionFormTemplateNavigation.name} (${inspections[i].pkNavigation.code}) \n${MyGlobal().dateFormat(inspections[i].inspectionDate)}" ??
                            '-',
                    location: inspections[i].pkNavigation.location ?? '-',
                    issuedBy: inspections[i].issuedByName ?? '-',
                    issuedDate: MyGlobal().dateFormat(inspections[i].issuedDate) ?? '-',
                    workTitle: inspections[i].pkPpmActivityNavigation.workTitle,
                    floor: inspections[i].pkPpmActivityNavigation.pkFloorNavigation.name,
                    selectedPk: inspections[i].pk,
                  ),
                ))),
        Container(
          height: isLoading ? 100 : 0,
          color: Colors.transparent,
          child: Center(
            child: new CircularProgressIndicator(),
          ),
        ),
      ],
    );
  }
}

class EForms extends StatelessWidget {
  final String formName;
  final String location;
  final String issuedBy;
  final String issuedDate;
  final String selectedPk;
  final String floor;
  final String workTitle;

  EForms(
      {this.formName,
      this.location,
      this.issuedBy,
      this.issuedDate,
      this.selectedPk,
      this.floor,
      this.workTitle});

  @override
  Widget build(BuildContext context) {

    void navigate(String selectedPk) {
      Navigator.pushNamed(context, Paths.eform);
       appData.selectedPk = selectedPk;
    }

    return GestureDetector(
      onTap: () => navigate(selectedPk),
      child: Container(
          padding: EdgeInsets.symmetric(horizontal: 30),
          margin: EdgeInsets.symmetric(vertical: 5),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              boxShadow: [
                BoxShadow(
                  color: Style.color3.withOpacity(0.2),
                  spreadRadius: 3,
                  blurRadius: 4,
                  offset: Offset(3, 4),
                ),
              ],
              color: Colors.white),
          height: 175,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(padding: EdgeInsets.only(top: 10)),
              Container(
                constraints: new BoxConstraints(
                    maxWidth: MediaQuery.of(context).size.width - 84),
                child: Text(formName.toUpperCase(),
                    style: Style.titleTextStyle
                        .copyWith(color: Colors.black, fontSize: 15)),
              ),
              Container(
                constraints: new BoxConstraints(
                    maxWidth: MediaQuery.of(context).size.width - 84),
                child: Text(
                  "$workTitle ($floor)",
                  style: Style.headerTextStyle
                      .copyWith(color: Colors.grey[800], fontSize: 15),
                ),
              ),
              Container(
                constraints: new BoxConstraints(
                    maxWidth: MediaQuery.of(context).size.width - 84),
                child: Text(
                  location,
                  style: Style.headerTextStyle
                      .copyWith(color: Colors.grey[800], fontSize: 15),
                ),
              ),
              Divider(),
              Container(
                constraints: new BoxConstraints(
                    maxWidth: MediaQuery.of(context).size.width - 84),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      child: Row(
                        children: <Widget>[
                          Icon(Icons.person),
                          Text(
                            issuedBy,
                            style: Style.headerTextStyle
                                .copyWith(color: Colors.grey[800], fontSize: 10),
                          )
                        ],
                      ),
                    ),
                    Container(
                      child: Row(
                        children: <Widget>[
                          Icon(Icons.date_range),
                          Text(
                            issuedDate,
                            style: Style.headerTextStyle
                                .copyWith(color: Colors.grey[800], fontSize: 10),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              )
            ],
          )),
    );
  }
}
